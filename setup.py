from setuptools import setup

setup(
    name='dry_pipe',
    version='0.1',
    packages=['dry_pipe'],
    install_requires=[
        'click==7.1.2',
        'uvicorn==0.13.4',
        'python-socketio==5.0.4',
        'psutil==5.8.0',
        'rich==9.10.0',
        'paramiko==2.7.2',
        'PyYAML==5.4.1',
        'requests==2.27.1'
    ],
    package_data={'': ['ui/dist/main.js']},
#    entry_points='''
#        [console_scripts]
#        drypipe=dry_pipe.cli:cli
#    ''',
)