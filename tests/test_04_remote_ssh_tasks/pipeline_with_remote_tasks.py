import os

from dry_pipe import DryPipe


def create_pipeline_with_remote_tasks(dynamic_conf, use_remote_code_dir=False):

    dsl = DryPipe.dsl(
        pipeline_instance_dir=os.path.dirname(__file__),
        pipeline_code_dir=os.path.join(
            os.path.dirname(__file__), "src"
        )
    )

    def gen_tasks(dsl):

        local_task = dsl.task(
            key="local_task"
        ).produces(
            precious_output=dsl.file("name-of-pipeline-host.txt")
        ).calls(
            "xyz/write-hostname-to-file-other-script.sh"
        )()

        yield local_task

        remote_task = dsl.task(
            key="remote_task",
            dynamic_conf=dynamic_conf
        ).depends_on(
            name_of_pipeline_host=local_task.out.precious_output
            #TODO: add var dep
        ).produces(
            precious_output=dsl.file("precious-remote-output.txt")
        ).calls(
            "xyz/write-hostname-to-file-other-script.sh" if use_remote_code_dir else """
            #!/usr/bin/env bash
            
            if [[ "${PLEASE_CRASH}" ]]; then
              exit 1
            fi
            
            if [[ ! -z "${name_of_pipeline_host+x}" ]]; then
              cat $name_of_pipeline_host >> $precious_output
            fi
            
            echo "hello from $(cat /etc/hostname)" >> $precious_output        
        """)()

        yield remote_task

    return DryPipe.create_pipeline(lambda: gen_tasks(dsl))

