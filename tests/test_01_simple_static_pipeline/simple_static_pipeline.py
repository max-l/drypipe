import os

from dry_pipe import DryPipe, cli


def all_pipeline_tasks(dsl):

    blast = dsl.task(
        key="blast.1",
        extra_env={
            "dummy_env_var": "abc"
        }
    ).depends_on(
        subject=dsl.file("human.fasta"),
        query=dsl.file("chimp.fasta"),
        fake_blast_output=dsl.file("human_chimp_blast-fake.tsv")
    ).produces(
        blast_out=dsl.file("human_chimp_blast.tsv"),
        v1=dsl.var(int),
        v2=dsl.var(float)
    ).calls("call_blast.sh")()

    yield blast

    report_task = dsl.task(
        key="report",
    ).depends_on(
        blast_result=blast.out.blast_out,
        v1_from_blast=blast.out.v1
    ).produces(
        fancy_report=dsl.file("fancy_report.txt"),
        x=dsl.var(int),
        s1=dsl.var(str),
        s2=dsl.var(type=str, may_be_none=True)
    ).calls("report.sh")()

    yield report_task

    # should not blow up:
    if report_task.out.s2.producing_task is None:
        raise Exception("report_task.out.fancy_vars.s2 should not be None !")

    yield dsl.task(
        key="python_much_fancier_report.1"
    ).depends_on(
        fancy_report=report_task.out.fancy_report,
        fancy_int=dsl.val(1),
        vx=report_task.out.x,
        vs1=report_task.out.s1,
        vs2=report_task.out.s2,
        v1=blast.out.v1,
        v2=blast.out.v2
    ).produces(
        much_fancier_report=dsl.file("fancier_report1.txt")
    ).calls(
        much_fancier_report_func
    )()

    yield dsl.task(
        key="python_much_much_fancier_report.2"
    ).depends_on(
        fancy_report=report_task.out.fancy_report,
        fancy_int=dsl.val(1000),
        vx=report_task.out.x,
        vs1=report_task.out.s1,
        vs2=report_task.out.s2
    ).produces(
        much_fancier_report=dsl.file("fancier_report2.txt")
    ).calls(
        much_fancier_super_report_func
    )()


@DryPipe.python_task
def much_fancier_report_func(much_fancier_report, fancy_int):

    print("got args:")
    print(f"much_fancier_report={much_fancier_report}")
    print(f"fancy_int={fancy_int}")

    with open(much_fancier_report, "w") as out:
        out.write("yooozzzz\n")
        out.write(f"--->{fancy_int}\n")

    return {
        "super_duper_int": fancy_int,
        "and_another_var": "wow..."
    }


@DryPipe.python_task
def much_fancier_super_report_func(much_fancier_report):

    with open(much_fancier_report, "w") as f:
        f.write("ultra fancy report...")


    return {
        "a": 321
    }


if __name__ == '__main__':

    dsl = DryPipe.dsl(pipeline_code_dir=os.path.dirname(__file__))

    cli.run(
        lambda: all_pipeline_tasks(dsl)
    )
