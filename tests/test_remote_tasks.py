import os
import unittest

import test_helpers
from dry_pipe import env_variables, DryPipe
from dry_pipe.janitors import Janitor
from test_04_remote_ssh_tasks import pipeline_with_remote_tasks


class RemoteTaskTestsBase(unittest.TestCase):

    def remote_tasks_basics(self, dynamic_conf, use_remote_code_dir=False, test_remote_dir=None):

        pipeline = pipeline_with_remote_tasks.create_pipeline_with_remote_tasks(
            dynamic_conf,
            use_remote_code_dir=use_remote_code_dir
        )

        pipeline.clean_all()

        for rex in pipeline.tasks.remote_executors():
            rex.ensure_connected()
            rex.ssh_client().exec_command(f"rm -Rf {test_remote_dir or env_variables.test_remote_dir()}/test_04_remote_ssh_tasks")
            rex.ssh_client().exec_command(f"mkdir {test_remote_dir or env_variables.test_remote_dir()}/test_04_remote_ssh_tasks")

        Janitor.work_sync_until_done(pipeline)

        local_task, remote_task = pipeline.tasks

        local_task_result = test_helpers.load_file_as_string(os.path.join(
            local_task.v_abs_work_dir(),
            "name-of-pipeline-host.txt"
        ))

        self.assertTrue("hello" in local_task_result)

        remote_task_result = test_helpers.load_file_as_string(os.path.join(
            remote_task.v_abs_work_dir(),
            "precious-remote-output.txt"
        )).strip()

        self.assertTrue(local_task_result in remote_task_result)

        self.assertTrue(len(remote_task_result.split("\n")) == 2)

def ignore():
    class Executer:

        def __init__(self, **kwargs):
            self.dir = ""

        def clone(self, **kwargs):
            return {}


    default_executer = Executer(
        host="localhost",
        dir=None, # optional for non remote, becomes parent of __pipeline_instance_dir
        task_runner="process"
    )

    ip29_process_executer = Executer(
        host="maxl@ip32.ccs.usherbrooke.ca:~/.ssh/id_rsa",
        dir="/nfs3_ib/ip32-ib/home/maxl/drypipe-tests",
        task_runner="process"
    )

    ip29_process_executer_other_code_dir = ip29_process_executer.clone(
        pipeline_code_dir=f"{ip29_process_executer.dir}/remote_code_dir_test_04"
    )

    ip29_process_executer_with_singularity_container = ip29_process_executer.clone(
        container="singularity-test-container.sif",
        containers_dir="/home/maxl/containers",
        # call at start of setenv.sh
        # should replace overrides.sh
        init_script="""
            #!/usr/bin/env bash
            module add singularity    
        """
    )

    ip29_slurm_executer = ip29_process_executer.clone(
        task_runner="slurm",
        slurm_account="def-xroucou_cpu",
        sbatch_options=["--time=0:5:00"]
    )

    ip29_slurm_executer_with_singularity_container = ip29_slurm_executer.clone(
        container="singularity-test-container.sif",
        containers_dir="/home/maxl/containers",
        init_script="""
            #!/usr/bin/env bash
            module add singularity    
        """
    )


class RemoteTaskTests1(RemoteTaskTestsBase):
    def test_remote_tasks_basics(self):
        self.remote_tasks_basics(
            DryPipe.dynamic_conf(
                executer_type="process",
                ssh_specs=
                    f"maxl@ip32.ccs.usherbrooke.ca:" +
                    f"/nfs3_ib/ip32-ib/home/maxl/drypipe-tests:~/.ssh/id_rsa"
            )
        )

    def test_remote_tasks_using_remote_code_dir(self):
        self.remote_tasks_basics(
            DryPipe.dynamic_conf(
                executer_type="process",
                ssh_specs=
                    "maxl@ip32.ccs.usherbrooke.ca:" +
                    "/nfs3_ib/ip32-ib/home/maxl/drypipe-tests:~/.ssh/id_rsa",
                remote_pipeline_code_dir="/nfs3_ib/ip32-ib/home/maxl/drypipe-tests/remote_code_dir_test_04"
            ),
            use_remote_code_dir=True
        )


class RemoteTaskTests2(RemoteTaskTestsBase):

    def test_remote_tasks_basics_with_container(self):

        self.remote_tasks_basics(
            DryPipe.dynamic_conf(
                executer_type="process",
                ssh_specs=
                    "maxl@ip29.ccs.usherbrooke.ca:/home/maxl/drypipe_tests:~/.ssh/id_rsa",
                container="singularity-test-container.sif",
                containers_dir="/home/maxl/containers",
                command_before_launch_container="module add singularity",
            ),
            test_remote_dir="/home/maxl/drypipe_tests"
        )


class RemoteTaskTestsWithSlurm(RemoteTaskTestsBase):

    def test_remote_tasks_with_slurm(self):
        self.remote_tasks_basics(
            DryPipe.dynamic_conf(
                executer_type="slurm",
                slurm_account="def-xroucou_cpu",
                sbatch_options=[
                    "--time=0:5:00"
                ],
                ssh_specs=
                    "maxl@ip32.ccs.usherbrooke.ca:/nfs3_ib/ip32-ib/home/maxl/drypipe-tests:~/.ssh/id_rsa",
            )
        )

    def test_remote_tasks_with_slurm_and_container(self):
        self.remote_tasks_basics(
            DryPipe.dynamic_conf(
                executer_type="slurm",
                slurm_account="def-xroucou_cpu",
                sbatch_options=[
                    "--time=0:5:00"
                ],
                ssh_specs="maxl@ip29.ccs.usherbrooke.ca:/home/maxl/drypipe-tests:~/.ssh/id_rsa",
                container="singularity-test-container.sif",
                containers_dir="/home/maxl/containers"
            ),
            test_remote_dir="/home/maxl/drypipe-tests"
        )
