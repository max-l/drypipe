import glob
import os
import pathlib
import shutil
import unittest

import test_helpers
from dry_pipe import DryPipe
from dry_pipe import janitors
from dry_pipe.actions import TaskAction
from dry_pipe.janitors import Janitor
from dry_pipe.task_state import VALID_TRANSITIONS
from test_00_ground_level_tests import pipeline_defs
from test_00_ground_level_tests.pipeline_defs import pipeline_exerciser


class GroundLevelTests(unittest.TestCase):

    def test_task_action_sanity(self):

        restart_0 = TaskAction.load_from_file("/action.restart")

        self.assertEqual("restart", restart_0.action_name)
        self.assertEqual(0, restart_0.step)

        restart_3 = TaskAction.load_from_file("/action.restart-3")

        self.assertEqual("restart", restart_3.action_name)
        self.assertEqual(3, restart_3.step)
        self.assertTrue(restart_3.is_restart())

        pause = TaskAction.load_from_file("/x/y/action.pause")

        self.assertEqual("pause", pause.action_name)
        self.assertIsNone(pause.step)
        self.assertTrue(pause.is_pause())

        shutil.rmtree("/tmp/.drypipe", ignore_errors=True)

        for control_dir in ["z0", "z2"]:
            pathlib.Path(os.path.join("/tmp", ".drypipe", control_dir)).mkdir(parents=True)

        TaskAction.submit("/tmp", "z0", "restart", 0)

        r0 = TaskAction.load_from_task_control_dir(os.path.join("/tmp", ".drypipe", "z0"))

        self.assertIsNotNone(r0)
        self.assertEqual(0, r0.step)

        self.assertRaises(Exception, lambda: TaskAction.submit("/tmp", "z0", "restart", 0))
        self.assertRaises(Exception, lambda: TaskAction.submit("/tmp", "z0", "restart", 2))

        TaskAction.submit("/tmp", "z2", "restart", 2)

        r2 = TaskAction.load_from_task_control_dir(os.path.join("/tmp", ".drypipe", "z2"))

        self.assertIsNotNone(r2)
        self.assertEqual(2, r2.step)


    def test_janitor_state_machine_sanity(self):
        for state, transitions in VALID_TRANSITIONS.items():
            for next_state in transitions:
                if next_state not in VALID_TRANSITIONS:
                    raise Exception(
                        f"State machine VALID_TRANSITIONS has unknown transition {next_state}, from {state}"
                    )

    def _create_pipeline(self, pipeline_instance_dir, dsl_tweak_func=lambda i: i, pipeline_code_dir=None, use_snippet=None):

        pipeline_code_dir = os.path.dirname(pipeline_defs.__file__)

        d = os.path.join(
            pipeline_code_dir,
            pipeline_instance_dir
        )

        shutil.rmtree(d, ignore_errors=True)
        pathlib.Path(d).mkdir(parents=True)

        def match_expr(*args):
            for p in args:
                for f in glob.glob(os.path.join(pipeline_code_dir, p)):
                    yield f

        for file in match_expr("*.fasta"):
            shutil.copy(file, d)

        dsl = dsl_tweak_func(DryPipe.dsl(pipeline_code_dir=pipeline_code_dir, pipeline_instance_dir=d))

        pipeline = DryPipe.create_pipeline(lambda: pipeline_defs.single_task_pipeline(dsl, use_snippet))

        pipeline.ensure_work_dirs_initialized()
        pipeline.calc_pre_existing_files_signatures()

        return pipeline

    def test_transitions_of_tasks_without_failures_00(self):
        self._test_transitions_of_tasks_without_failures_01(use_snippet=False)

    def test_transitions_of_tasks_without_failures_01(self):
        self._test_transitions_of_tasks_without_failures_01(use_snippet=True)

    def _test_transitions_of_tasks_without_failures_01(self, use_snippet):

        pipeline = self._create_pipeline("p01", use_snippet=use_snippet)

        unique_task = list(pipeline.tasks)[0]

        p, work_done, b = janitors.main_janitor(pipeline, 0)
        state = unique_task.get_state()

        self.assertEqual(1, work_done)
        self.assertTrue(state.is_waiting_for_deps())

        p, work_done, b = janitors.main_janitor(pipeline, 0)
        state = unique_task.get_state()

        self.assertEqual(1, work_done)
        self.assertTrue(state.is_prepared())

        p, work_done, b = janitors.main_janitor(pipeline, 0)
        self.assertEqual(0, work_done)

        p, work_done = janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        self.assertEqual(2, work_done)

        def poll_task_state():
            task_state = unique_task.get_state()

            if not (task_state.is_launched() or task_state.is_step_started() or
                    task_state.is_step_completed() or task_state.is_completed_unsigned()):
                raise Exception(f"task state can't be {task_state.state_name}")

            return task_state.is_completed_unsigned()

        #test_helpers.poll_and_wait_until_true(poll_task_state)

        janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        state = unique_task.get_state()
        self.assertTrue(state.is_completed())

        out_vars = dict(unique_task.iterate_out_vars())

        self.assertEqual({"huge_variable": "123"}, out_vars)

        self.assertTrue(os.path.exists(
            unique_task.abs_path_of_produced_file("inflated-dummy.fasta")
        ))

    def test_transitions_of_tasks_with_failures_02_no_snippet(self):
        self._test_transitions_of_tasks_with_failures_02(use_snippet=False)

    def test_transitions_of_tasks_with_failures_02_with_snippet(self):
        self._test_transitions_of_tasks_with_failures_02(use_snippet=True)

    def _test_transitions_of_tasks_with_failures_02(self, use_snippet):

        pipeline = self._create_pipeline("p02", lambda dsl: dsl.with_defaults(
            extra_env={
                "PLEASE_CRASH": "single-task"
            }
        ),use_snippet=use_snippet)

        unique_task = list(pipeline.tasks)[0]

        # prepare task:
        janitors.main_janitor(pipeline, 0)
        janitors.main_janitor(pipeline, 0)

        p, work_done = janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True, fail_silently=True)

        self.assertEqual(2, work_done)

        def poll_task_state():

            task_state = unique_task.get_state()

            if not (task_state.is_launched() or task_state.is_step_started() or
                    task_state.is_failed()):
                raise Exception(f"task state can't be {task_state.state_name}")

            return task_state.is_failed()

        test_helpers.poll_and_wait_until_true(poll_task_state)

        task_state = unique_task.get_state()
        self.assertTrue(task_state.is_failed())

        p, work_done = janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)
        task_state = unique_task.get_state()

        self.assertEqual(0, work_done)
        self.assertTrue(task_state.is_failed())


class TaskSignatureTests(unittest.TestCase):

    def _test_03(self):

        pipeline, sig_loader, inputs_are_stale_loader = pipeline_exerciser(1, 2, 3, 4, "p03")

        Janitor.work_sync_until_done(pipeline)

        expected_signatures_1_2_3_4 = [
            '0f1f6df99896b5827f853e4ed25b7cb2a397d222', '222f805f51037a734dd95de732b20cb16fa3c7fc',
            'fddcefdc738f1134eb15a93ecb61b5045efbc04a', '213f3ee459c0b32741f89ee85a1dde1f7d590815'
        ]

        self.assertEqual(expected_signatures_1_2_3_4, sig_loader())

        self.assertEqual(
            [False, False],
            inputs_are_stale_loader()
        )

        pipeline, sig_loader, inputs_are_stale_loader = pipeline_exerciser(1, 12, 3, 4, "p03", skip_reset=True)

        pipeline.recompute_signatures()

        self.assertEqual(expected_signatures_1_2_3_4, sig_loader())

#        self.assertEqual(
#            [True, False],
#            inputs_are_stale_loader()
#        )

        pipeline, sig_loader, inputs_are_stale_loader = pipeline_exerciser(1, 2, 3, 4, "p03", skip_reset=True)

        pipeline.recompute_signatures()

        self.assertEqual(
            [False, False],
            inputs_are_stale_loader()
        )

        self.assertEqual(expected_signatures_1_2_3_4, sig_loader())

        pipeline, sig_loader, inputs_are_stale_loader = pipeline_exerciser(1, 2, 13, 4, "p03", skip_reset=True)

        pipeline.recompute_signatures()

        self.assertEqual(
            [False, True],
            inputs_are_stale_loader()
        )

        self.assertEqual(expected_signatures_1_2_3_4, sig_loader())
