import glob
import os
import sys
import pathlib
import shutil
import time
import unittest

import test_helpers
from dry_pipe.internals import ValidationError, ProducedFile
from dry_pipe import DryPipe, janitors
from dry_pipe.janitors import Janitor
from dry_pipe.task_state import TaskState
from test_01_simple_static_pipeline.simple_static_pipeline import all_pipeline_tasks
from test_helpers import singularity_conf, is_ip29, slurm_conf

dsl = DryPipe.dsl(pipeline_code_dir=__file__)

def before_execute_bash():
    if is_ip29():
        return "source /cvmfs/soft.computecanada.ca/config/profile/bash.sh"
    return None


def simple_static_pipeline_01_code_dir():
    d = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(d, "test_01_simple_static_pipeline")


dsl_4_simple_static_pipeline_01 = DryPipe.dsl(
    pipeline_code_dir=simple_static_pipeline_01_code_dir()
)

def create_pipeline(dsl):
    return DryPipe.create_pipeline(lambda: all_pipeline_tasks(dsl))


def create_tweakable_pipeline(dsl_tweak_func):
    dsl = dsl_tweak_func(dsl_4_simple_static_pipeline_01)
    pipeline = DryPipe.create_pipeline(lambda: all_pipeline_tasks(dsl))
    pipeline.annihilate_work_dirs()

    pipeline.init_work_dir_if_not_exists()


    return pipeline

class BaseTests(unittest.TestCase):

    def ensure_validation_error(self, func, code=None):

        try:
            func()
            raise Exception("ValidationError was not raised")
        except ValidationError as e:
            if code is None or e.code is None:
                return
            if code != e.code:
                raise Exception(f"Wrong ValidationError.code: {e.code}, expected {code} \n{e}")

    def ensure_exception_thrown(self, func, code=None):

        try:
            func()
            raise Exception("Exception was not raised")
        except Exception as e:
            if code is None or e.code is None:
                return

    def test_task_construction_sanity(self):

        self.ensure_validation_error(lambda: dsl.val([]))
        self.ensure_validation_error(lambda: dsl.file(123))

        self.ensure_validation_error(lambda: dsl.task(key="k1", executer={"p": dsl.slurm(account="z")}))
        self.ensure_validation_error(lambda: dsl.task(key="k1", executer={"p": dsl.val("")}))
        self.ensure_validation_error(lambda: dsl.task(key="k1", executer=dsl.singularity("zaz.sif")))

        # good tasks:

        dsl.task(
            key="k2",
            executer=dsl.slurm(account="z"),
            container=dsl.singularity(image_path="zaz.sif")
        )

        dsl.task(
            key="k2",
            executer=dsl.local()
        )

        DryPipe.dsl(pipeline_code_dir=".").with_defaults(executer=dsl.slurm(account="z"), pipeline_instance_dir=".")

        dp2 = DryPipe.dsl(pipeline_code_dir=".").with_defaults(
            pipeline_instance_dir=".",
            executer=dsl.local(),
            extra_env={"a": 1},
            container=dsl.singularity("tst.sif")
        )


        t1 = dp2.task(
            key="k"
            #publish_dir="zaz"
        )

        self.assertEqual(t1.container.image_path, "tst.sif")


    def test_depends_on_sanity(self):

        self.ensure_validation_error(
            lambda: dsl.task(key="t1").depends_on(123),
            ValidationError.depends_on_has_bad_positional_arg
        )

        self.ensure_validation_error(
            lambda: dsl.task(key="t1").depends_on(dsl.val(123)),
            ValidationError.depends_on_has_bad_positional_arg_val
        )

        self.ensure_validation_error(
            lambda: dsl.task(key="t1").depends_on(x=123),
            ValidationError.depends_on_has_invalid_kwarg_type
        )

        dsl.task(key="t1").depends_on(x=dsl.val(123))

        dsl.task(key="t1").depends_on(x=dsl.file("a123.txt"))


    def test_produces_sanity(self):

        self.ensure_validation_error(
            lambda: dsl.task(key="t1").produces(123),
            ValidationError.produces_cant_take_positional_args
        )


        dsl.task(key="t1").produces(x=dsl.var(123))


        dsl.task(key="t1").produces(x=dsl.file("a.txt"))

    def test_referring_to_produced_files(self):

        t1 = dsl.task("t1").produces(f1=dsl.file("f.txt")).calls("a.sh")()

        self.ensure_validation_error(lambda: t1.out.missing)

        # should not blow up:
        f1 = t1.out.f1

        self.ensure_exception_thrown(lambda: t1.f1)



        self.assertIsInstance(f1, ProducedFile)

        self.assertEqual("f.txt", f1.file_path)

        self.assertEqual("f1", f1.var_name)


    def test_pipeline_definition_sanity(self):

        dp = DryPipe.dsl(
            pipeline_code_dir="."
        )

        def definition_with_key_collision():
            yield dp.task(key="k1")
            yield dp.task(key="k1")

        self.assertRaises(ValidationError, lambda: DryPipe.create_pipeline(definition_with_key_collision))

        def returns_no_iterable():
            return 2

        self.assertRaises(ValidationError, lambda: DryPipe.create_pipeline(returns_no_iterable))


        def returns_non_task():
            yield 234

        self.assertRaises(ValidationError, lambda: DryPipe.create_pipeline(returns_non_task))


class NonTrivialPipelineTests(unittest.TestCase):

    def test_steps_pipeline_01(self):

        dsl = dsl_4_simple_static_pipeline_01.with_defaults(
            executer=dsl_4_simple_static_pipeline_01.local(),
            container=None
        )

        pipeline = create_pipeline(dsl)

        blast, report_task, python_much_fancier_report, python_much_much_fancier_report = pipeline.tasks

        upstream_deps_list = list(python_much_fancier_report.upstream_deps_iterator())

        (blast_ref, empty_file_deps, blast_ref_var_deps),\
        (report_task_ref, report_task_file_deps, report_task_var_deps) = upstream_deps_list

        self.assertEqual(blast, blast_ref)
        self.assertEqual(report_task, report_task_ref)

        self.assertEqual(len(empty_file_deps), 0)
        self.assertEqual(len(blast_ref_var_deps), 2)

        v1, v2 = blast_ref_var_deps

        self.assertEqual(v1.var_name_in_consuming_task, "v1")
        self.assertEqual(v2.var_name_in_consuming_task, "v2")

        self.assertEqual(len(report_task_file_deps), 1)
        self.assertEqual(len(report_task_var_deps), 3)

        fancy_report = report_task_file_deps[0]
        self.assertEqual(fancy_report.var_name_in_consuming_task, "fancy_report")

        self.assertDictEqual(
            vars(fancy_report.produced_file),
            {
                "file_path": 'fancy_report.txt',
                "manage_signature": None,
                'is_dummy': False,
                "producing_task": report_task,
                "var_name": 'fancy_report'
            }
        )

        self.assertEqual(
            ["vs1", "vs2", "vx"],
            list(map(lambda v: v.var_name_in_consuming_task, report_task_var_deps))
        )

        self.assertEqual(
            ['s1', 's2', 'x'],
            list(map(lambda v: v.output_var.name, report_task_var_deps))
        )

        for v in report_task_var_deps:
            self.assertIsNotNone(v.output_var.producing_task)

        self.assertEqual(
            [False, True, False],
            list(map(lambda v: v.output_var.may_be_none, report_task_var_deps))
        )

        self.assertEqual(
            [str, str, int],
            list(map(lambda v: v.output_var.type, report_task_var_deps))
        )

    def test_pipeline_orchestration(self):

        pipeline = create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
            executer=dsl.local(),
            container=None,
            python_bin=python_bin_containerless()
        ))

        blast, report_task, python_much_fancier_report, python_much_much_fancier_report = pipeline.tasks

        janitors.main_janitor(pipeline, 0)
        janitors.main_janitor(pipeline, 0)

        prepared_task_states = list(TaskState.prepared_task_states(pipeline))

        self.assertEqual(len(prepared_task_states), 1)
        self.assertEqual(prepared_task_states[0].task_key, blast.key)


        test_helpers.janitor_until_task_completed(pipeline, blast)

        test_helpers.janitor_until_task_completed(pipeline, report_task)


        self.assertEqual(
            {s.task_key for s in TaskState.waiting_for_deps_task_states(pipeline)},
            {python_much_fancier_report.key, python_much_much_fancier_report.key}
        )

        test_helpers.janitor_until_task_completed(pipeline, python_much_fancier_report)

        test_helpers.janitor_until_task_completed(pipeline, python_much_much_fancier_report)

    def test_pipeline_graph(self):

        pipeline = create_pipeline(dsl_4_simple_static_pipeline_01.with_defaults(
            executer=dsl.local(),
            container=None,
            python_bin=python_bin_containerless()
        ))

        tasks, deps = pipeline.summarized_dependency_graph()


class WithManyConfigCombinationsTests(unittest.TestCase):

    def validate_task_control(self, pipeline, uses_singularity, is_slurm):

        blast, report_task, python_much_fancier_report, python_much_much_fancier_report = pipeline.tasks

        d = list(blast.iterate_unsatisfied_deps())
        self.assertEqual(len(d), 0)

        missing_deps = list(report_task.iterate_unsatisfied_deps())

        self.assertEqual(len(missing_deps), 1)

        (msg, code, task, missing_file_deps, missing_var_deps) = missing_deps[0]

        self.assertEqual(task, blast)
        self.assertEqual(len(missing_file_deps), 1)
        self.assertEqual(len(missing_var_deps), 1)

        Janitor.work_sync_until_done(pipeline)

        self.assertEqual(
            "9a380a5deaa3a091368d3a07b722d5362328a07b",
            blast.output_signature()
        )

        self.assertEqual(
            "a336aef80f2358c60f16e1e5a10839b59a5e2e42",
            report_task.output_signature()
        )

        self.assertEqual(
            "e31ead308b82c9ccbf67791517f1bb864bf29840",
            python_much_fancier_report.output_signature()
        )

        self.assertEqual(
            "44a6b33b025d65f155ca1c3431b4478924de77eb",
            python_much_much_fancier_report.output_signature()
        )


    def _test_launch_watch_and_launch(self):

        pipeline = create_pipeline(dsl_4_simple_static_pipeline_01.with_defaults(
            executer=dsl.local(),
            container=None,
            python_bin=python_bin_containerless()
        ))

        pipeline.clean()

        pipeline.launch_watch_and_launch(sleep_interval=2)


class NonTrivialPipelineLocalContainerlessTests(WithManyConfigCombinationsTests):

    def test_non_trivial_local_containerless(self):

        self.validate_task_control(
            create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
                executer=dsl.local(),
                container=None,
                python_bin=python_bin_containerless()
            )),
            uses_singularity=False,
            is_slurm=False
        )


class NonTrivialPipelineLocalWithSingularityContainerTests(WithManyConfigCombinationsTests):

    def test_non_trivial_local_with_singularity(self):

        self.validate_task_control(
            create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
                executer=dsl.local(
                    before_execute_bash=before_execute_bash()
                ),
                container=singularity_conf(dsl),
                python_bin="/usr/bin/python3"
            )),
            uses_singularity=True,
            is_slurm=False
        )


class NonTrivialPipelineSlurmContainerlessTests(WithManyConfigCombinationsTests):

    def test_non_trivial_slurm_containerless(self):

        self.validate_task_control(
            create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
                executer=slurm_conf(dsl),
                container=None,
                python_bin=python_bin_containerless()
            )),
            uses_singularity=False,
            is_slurm=True
        )


class NonTrivialPipelineSlurmWithSingularityContainerTests(WithManyConfigCombinationsTests):

    def test_non_trivial_slurm_with_singularity(self):

        self.validate_task_control(
            create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
                executer=slurm_conf(dsl),
                container=singularity_conf(dsl_4_simple_static_pipeline_01),
                python_bin="/usr/bin/python3"
            )),
            uses_singularity=True,
            is_slurm=True
        )


class NonTrivialPipelineCodeDirAndInstanceDirDistinctTests(WithManyConfigCombinationsTests):

    def test_code_dir_and_instance_dir_distinct(self):

        code_dir = simple_static_pipeline_01_code_dir()
        instance_dir_for_test = os.path.join(simple_static_pipeline_01_code_dir(), "instance_dir_for_test")
        shutil.rmtree(instance_dir_for_test, ignore_errors=True)
        pathlib.Path(instance_dir_for_test).mkdir()

        # data files to copy in the instance dir
        # note: code files (call_blast.sh report.sh) dont get copied
        data_files_to_copy = [
            os.path.join(code_dir, f)
            for f in [
                "chimp.fasta",
                "human.fasta",
                "human_chimp_blast-fake.tsv"
            ]
        ]

        for f in data_files_to_copy:
            shutil.copy(f, instance_dir_for_test)

        self.validate_task_control(
            create_tweakable_pipeline(lambda dsl: dsl.with_defaults(
                executer=dsl.local(),
                container=None,
                python_bin=python_bin_containerless(),
                pipeline_instance_dir=instance_dir_for_test
            )),
            uses_singularity=False,
            is_slurm=False
        )



def attempt_seqence(n, initial_sleep, sleep_interval_after, sleep_max, expire_msg):

    time.sleep(initial_sleep)

    for i in range(0, n):

        yield i+1

        time.sleep(sleep_interval_after)

    raise Exception(f"{n} attempts made, and failed: {expire_msg}")


def python_bin_containerless():
    return sys.executable
