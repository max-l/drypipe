import os

from dry_pipe import DryPipe, cli

dsl = DryPipe.dsl(pipeline_code_dir=os.path.dirname(__file__))


def single_infinite_task_pipeline(with_slurm=True):

    if with_slurm:
        executer = dsl.slurm(
            account="def-rodrigu1",
            sbatch_options=[
                "--time=0:1:00"
            ]
        )
    else:
        executer = dsl.local()

    yield dsl.task(
        key="loop-n-times",
        executer=executer
    ).depends_on(
        count=dsl.val(10000)
    ).produces(
        result=dsl.var(int)
    ).calls("loop-n-times.sh")()


if __name__ == '__main__':

    cli.run()
