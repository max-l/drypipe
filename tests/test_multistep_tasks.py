import unittest

import os

import test_helpers
from dry_pipe.janitors import Janitor

from test_03_multistep_tasks import pipeline_with_multistep_tasks
from test_03_multistep_tasks.pipeline_with_multistep_tasks import create_pipeline_with_mixed_python_bash, \
    create_pipeline_with_mixed_executers


class MultipstepTaskTests(unittest.TestCase):

    def test_normal_execution(self):

        pipeline = pipeline_with_multistep_tasks.create_three_steps_pipeline()

        pipeline.clean_all()

        Janitor.work_sync_until_done(pipeline)

        three_phase_task = next(pipeline.tasks_for_key_prefix("three_phase_task"))

        three_step_task_out_file = os.path.join(three_phase_task.v_abs_work_dir(), "out_file.txt")

        def load_output_from_three_phase_task():
            return test_helpers.load_file_as_string(three_step_task_out_file)

        def validate_output_of_three_phase_task():
            out = load_output_from_three_phase_task()
            self.assertEqual("s1\ns2\ns3\n", out)

        validate_output_of_three_phase_task()

        pipeline.clean_all()

        from dry_pipe.internals import Local
        try:
            Local.fail_silently_for_test = True

            Janitor.work_sync_until_done(pipeline, extra_env={
                "CRASH_STEP_2": "true"
            })

            self.assertEqual("s1\n", load_output_from_three_phase_task())
            self.assertTrue(three_phase_task.get_state().is_failed())

            test_helpers.rewrite_file(three_step_task_out_file, "")

            three_phase_task.re_queue()

            self.assertFalse(three_phase_task.get_state().is_failed())

            Janitor.work_sync_until_done(pipeline, extra_env={
                "CRASH_STEP_3": "true"
            })

            # ensure s1 is not there, i.e. step1 has NOT executed
            self.assertEqual("s2\n", load_output_from_three_phase_task())

            three_phase_task.re_queue()

            Janitor.work_sync_until_done(pipeline, extra_env={})

            self.assertEqual("s2\ns3\n", load_output_from_three_phase_task())

            """
            event_lines = list(three_phase_task.parse_tracking_file())
    
            step_lines = [
                int(l.split("_")[1])
                for l in event_lines if l.startswith("step_")
            ]
    
            self.assertEqual(step_lines, [0, 0, 1, 1, 2, 2])
    
            event_lines = [
                l.split("=")[0]
                for l in event_lines
            ]
    
            self.assertTrue("launched_at" in event_lines)
            self.assertTrue("completed_at" in event_lines)
            """
        finally:
            Local.fail_silently_for_test = False

    def test_pipeline_with_mixed_python_bash(self):

        pipeline = create_pipeline_with_mixed_python_bash()

        pipeline.clean()

        Janitor.work_sync_until_done(pipeline)

        three_phase_task = next(pipeline.tasks_for_key_prefix("three_phase_task"))

        three_phase_task_out_file = os.path.join(three_phase_task.v_abs_work_dir(), "out_file.txt")

        self.assertEqual("s1\ns2\ns3\ns4\n", test_helpers.load_file_as_string(three_phase_task_out_file))

    def _pipeline_with_mixed_executers(self):

        pipeline = create_pipeline_with_mixed_executers()

        pipeline.clean()

        pipeline.launch_watch_and_launch(sleep_interval=1)

        three_phase_task = next(pipeline.tasks_for_key_prefix("three_phase_task"))

        three_phase_task_out_file = os.path.join(three_phase_task.v_abs_work_dir(), "out_file.txt")

        self.assertEqual("s1\ns2\ns3\ns4\n", test_helpers.load_file_as_string(three_phase_task_out_file))
