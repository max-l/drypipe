import os

from dry_pipe import DryPipe, cli

dsl = DryPipe.dsl(pipeline_code_dir=os.path.dirname(__file__))


def create_three_steps_pipeline(tweak_defaults_func=lambda v: v):

    tweaked_dsl = tweak_defaults_func(dsl)

    def gen_tasks(dsl):

        three_phase_task = dsl.task(
            key="three_phase_task"
        ).produces(
            out_file=dsl.file("out_file.txt")
        ).calls(
            "step1.sh"
        ).calls(
            "step2.sh"
        ).calls(
            "step3.sh"
        )()

        yield three_phase_task

    return DryPipe.create_pipeline(lambda: gen_tasks(tweaked_dsl))


@DryPipe.python_task
def step2_in_python(out_file):
    with open(out_file, "a") as f:
        f.write("s2\n")

@DryPipe.python_task
def step4_in_python(out_file):
    with open(out_file, "a") as f:
        f.write("s4\n")


def create_pipeline_with_mixed_python_bash(tweak_defaults_func=lambda v: v):

    import test_helpers

    tweaked_dsl = tweak_defaults_func(dsl)

    def gen_tasks(dsl):

        three_phase_task = dsl.task(
            key="three_phase_task"
        ).produces(
            out_file=dsl.file("out_file.txt")
        ).calls(
            "step1.sh",
            container=test_helpers.singularity_conf(dsl)
        ).calls(
            step2_in_python,
            container=test_helpers.singularity_conf(dsl)
        ).calls(
            "step3.sh"
        ).calls(
            step4_in_python
        )()

        yield three_phase_task

    return DryPipe.create_pipeline(lambda: gen_tasks(tweaked_dsl))


def create_pipeline_with_mixed_executers(tweak_defaults_func=lambda v: v):

    import test_helpers

    tweaked_dsl = tweak_defaults_func(dsl)

    def gen_tasks(dsl):

        three_phase_task = dsl.task(
            key="three_phase_task"
        ).produces(
            out_file=dsl.file("out_file.txt")
        ).calls(
            "step1.sh"
        ).calls(
            "step2.sh",
            executer=test_helpers.slurm_conf(dsl)
        ).calls(
            "step3.sh"
        ).calls(
            step4_in_python
        )()

        yield three_phase_task

    return DryPipe.create_pipeline(lambda: gen_tasks(tweaked_dsl))

if __name__ == '__main__':

    cli.run()
