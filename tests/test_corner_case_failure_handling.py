import unittest

from dry_pipe import DryPipe
from dry_pipe.janitors import Janitor
from test_06_zombie_detection.pipeline_defs import single_infinite_task_pipeline


class CornerCasesFailureTests(unittest.TestCase):

    def test_slurm_timeout_signal_handling(self):

        pipeline = DryPipe.create_pipeline(lambda: single_infinite_task_pipeline(with_slurm=True))

        single_task, = pipeline.tasks

        pipeline.clean()

        Janitor.work_sync_until_done(pipeline)

        self.assertTrue(single_task.get_state().is_timed_out())


