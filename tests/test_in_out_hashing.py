import os
import shutil
import unittest

import test_helpers as th
from dry_pipe import DryPipe
from test_02_dynamic_dep_graph import pipeline_with_dynamic_dep_graph
from test_02_dynamic_dep_graph.pipeline_with_dynamic_dep_graph import get_expected_agg_result


def create_test_pipeline(tweak_defaults_func=lambda v: v):

    tweaked_dsl = tweak_defaults_func(
        DryPipe.dsl_for(pipeline_with_dynamic_dep_graph.all_pipeline_tasks)
    )

    work_dir = os.path.join(tweaked_dsl.pipeline_instance_dir, ".drypipe")
    publish_dir = os.path.join(tweaked_dsl.pipeline_instance_dir, "publish")

    shutil.rmtree(work_dir, ignore_errors=True)
    shutil.rmtree(publish_dir, ignore_errors=True)

    pipeline = DryPipe.create_pipeline(lambda: pipeline_with_dynamic_dep_graph.all_pipeline_tasks(tweaked_dsl))

    pipeline.init_work_dir_if_not_exists()

    return pipeline


class InOutHashingTests(unittest.TestCase):

    def test_input_hash_tracking(self):

        pipeline = create_test_pipeline()

        init_task, agg_task = pipeline.tasks

        th.janitor_until_task_completed(pipeline, agg_task)

        self.assertEqual(
            th.count(pipeline, lambda task: task.has_completed()),
            6
        )

        preparation_task = next(pipeline.tasks_for_key_prefix("preparation_task"))

        task_for_work_chunk_2 = next(pipeline.tasks_for_key_prefix("work_chunk.2"))

        self.assertEqual(task_for_work_chunk_2.key, "work_chunk.2")

        sig_chunk2_before = preparation_task.signature_of_produced_file("work_chunk.2.txt")

        th.rewrite_file(os.path.join(preparation_task.v_abs_work_dir(), "work_chunk.2.txt"), "23")

        # flag "stale" task signatures
        total_changed = pipeline.recompute_signatures()

        self.assertEqual(1, total_changed)

        self.assertTrue(task_for_work_chunk_2.is_input_signature_flagged_as_changed())

        self.assertNotEqual(
            sig_chunk2_before,
            preparation_task.signature_of_produced_file("work_chunk.2.txt")
        )

        task_for_work_chunk_2.get_state().transition_to_waiting_for_deps()

        # restart pipeline
        th.janitor_until_task_completed(pipeline, task_for_work_chunk_2)
        th.janitor_until_task_completed(pipeline, agg_task)

        total_changed = pipeline.recompute_signatures()

        task_for_work_chunk_2.write_output_signature_file()

        self.assertFalse(task_for_work_chunk_2.is_input_signature_flagged_as_changed())


    def _test_hash_tracking_only_check_end_result(self):

        pipeline = create_test_pipeline()

        init_task, agg_task = pipeline.tasks

        th.janitor_until_task_completed(pipeline, agg_task)

        self.assertEqual(
            th.count(pipeline, lambda task: task.has_completed()),
            6
        )

        preparation_task = next(pipeline.tasks_for_key_prefix("preparation_task"))

        th.rewrite_file(os.path.join(preparation_task.v_abs_work_dir(), "work_chunk.2.txt"), "23")

        # flag "dirty" tasks
        total_changed = pipeline.recompute_signatures()

        th.janitor_until_task_completed(pipeline, agg_task)

        aggregate_all_task = next(pipeline.tasks_for_key_prefix("aggregate_all"))

        self.assertNotEqual(842, get_expected_agg_result(aggregate_all_task))

