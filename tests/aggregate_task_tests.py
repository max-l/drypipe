import glob
import json
import os
import shutil
import psutil
import unittest

import test_helpers

from dry_pipe import janitors, DryPipe
from dry_pipe.janitors import Janitor
from test_02_dynamic_dep_graph import pipeline_with_dynamic_dep_graph
from test_02_dynamic_dep_graph.pipeline_with_dynamic_dep_graph import get_expected_agg_result


def create_test_pipeline(tweak_defaults_func=lambda v: v):

    tweaked_dsl = tweak_defaults_func(
        DryPipe.dsl_for(pipeline_with_dynamic_dep_graph.all_pipeline_tasks)
    )

    work_dir = os.path.join(tweaked_dsl.pipeline_instance_dir, ".drypipe")
    publish_dir = os.path.join(tweaked_dsl.pipeline_instance_dir, "publish")

    shutil.rmtree(work_dir, ignore_errors=True)
    shutil.rmtree(publish_dir, ignore_errors=True)

    pipeline = DryPipe.create_pipeline(lambda: pipeline_with_dynamic_dep_graph.all_pipeline_tasks(tweaked_dsl))

    return pipeline



class AggregateTaskTests(unittest.TestCase):


    def test_agg_task_pipeline(self):

        pipeline = create_test_pipeline()

        pipeline.init_work_dir_if_not_exists()

        self.assertEqual(len(pipeline.tasks), 2)

        init_task, agg_task = pipeline.tasks

        janitors.main_janitor(pipeline, 0)
        janitors.main_janitor(pipeline, 0)
        janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        janitors.auxiliary_janitor(pipeline, 0)
        janitors.main_janitor(pipeline, 0)
        janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        task_count_after_regen = len(pipeline.tasks)

        if task_count_after_regen != 6:
            self.fail(f"Expected 6 tasks after regen, got {task_count_after_regen}")

        janitors.main_janitor(pipeline, 0)
        janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        work_chunk_tasks = list(pipeline.tasks_for_glob_expr("work_chunk.*"))

        for t in work_chunk_tasks:
            test_helpers.janitor_until_task_completed(pipeline, t)

        janitors.main_janitor(pipeline, 0)
        janitors.auxiliary_janitor(pipeline, 0, wait_for_completion=True)

        expected_agg_result = get_expected_agg_result(agg_task)

        self.assertEqual(expected_agg_result, 20)

        self.assertEqual(1, sum([
            1
            for line in test_helpers.load_file_as_string(agg_task.v_abs_setenv_file()).split("\n")
            if line.startswith("export all_work_chunk_tasks_outputs=$__pipeline_instance_dir/publish/work_chunk.*")
        ]))


    def test_launch_watch_and_launch_agg_task(self):

        pipeline = create_test_pipeline()

        init_task, agg_task = pipeline.tasks

        Janitor.work_sync_until_done(pipeline)

        actual_agg_result = get_expected_agg_result(agg_task)

        self.assertEqual(actual_agg_result, 20)

    def test_agg_task_pipeline_graph(self):

        pipeline = create_test_pipeline()

        Janitor.work_sync_until_done(pipeline)

        g = pipeline.summarized_dependency_graph()

        r = json.dumps(g, indent=4)


    def obsolete_test_fake_fileset_iteration(self):
        pipeline = create_test_pipeline()
        pipeline.clean()
        tasks, deps = pipeline.summarized_dependency_graph(fake_splits=True)
        self.assertEqual(len(tasks), 3)
        self.assertEqual(len(deps), 2)
