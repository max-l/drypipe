import glob
import signal
import json
import os
import subprocess
import sys
import getpass
import threading
import logging.config
import re

import yaml
import psutil
from psutil import AccessDenied

from dry_pipe import env_variables
from dry_pipe.bash import bash_shebang
from dry_pipe.task_state import TaskState
from dry_pipe.utils import perf_logger_timer

LOCAL_PROCESS_IDENTIFIER_VAR = "____DRY_PIPE_TASK"

log_conf_file = env_variables.log_config()


def DEBUG_CONFIG(main_level, drypype_level, root_level):
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
            },
        },
        'loggers': {

            'zoz': {  # root logger
                'handlers': ['console'],
                'level': root_level,
                'propagate': False
            },
            'drypipe.*': {
                'handlers': ['console'],
                'level': drypype_level,
                'propagate': False
            },
            '__main__': {
                'handlers': ['console'],
                'level': main_level,
                'propagate': False
            }
        }
    }


def setup_debug_log_config(drypype_level="INFO", root_level="INFO", main_level="INFO", extra_loggers={}):
    print("DEBUG CONFIG")

    conf = DEBUG_CONFIG(main_level, drypype_level, root_level)

    conf["loggers"] = {
        ** conf["loggers"],
        ** extra_loggers
    }

    logging.config.dictConfig(conf)

if log_conf_file is not None:
    if log_conf_file == "DEBUG_CONFIG":
        setup_debug_log_config()
    else:
        with open(log_conf_file) as f:

            print(f"log config: {log_conf_file}")
            config = yaml.load(f, Loader=yaml.FullLoader)
            logging.config.dictConfig(config)


logger = logging.getLogger(__name__)


class ValidationError(Exception):
    def __init__(self, message, code=None):
        super(ValidationError, self).__init__(message)
        self.code = code

    depends_on_has_bad_positional_arg_val = 0

    depends_on_has_bad_positional_arg = 1

    depends_on_has_invalid_kwarg_type = 2

    produces_cant_take_positional_args = 3

    produces_only_takes_files = 4

    call_has_bad_arg = 5


class Val:

    def __init__(self, value):
        t = type(value)
        if t == int or t == str or t == float:
            self.value = value
        elif t == type(int):
            raise ValidationError(f"invalid val type dsl.val(int), you probably want to use 'dsl.var(int)'")
        elif t == t == type(str):
            raise ValidationError(f"invalid val type dsl.var(str), you probably want to use 'dsl.var(str)'")
        elif type(float):
            raise ValidationError(f"invalid val type, dsl.var(float) you probably want to use 'dsl.var(float)'")
        else:
            raise ValidationError(f"invalid val type: {type(value)}, must be int, string, float")

    def to_hash(self):
        return str(self.value)

    def serialized_value(self):

        if type(self.value) == str:
            return f'"{self.value}"'

        return f"{self.value}"


class IncompleteVar:

    def __init__(self, type=str, may_be_none=True):
        self.type = type
        self.may_be_none = may_be_none


class OutputVar:

    def __init__(self, type, may_be_none, name, producing_task):

        supported = [str, int, float]

        if type not in supported:
            raise ValidationError(f"var {name} can't be of type {type} supported types are: {','.join(supported)}")

        self.name = name
        self.type = type
        self.producing_task = producing_task
        self.may_be_none = may_be_none

    def parse(self, v):

        if self.type == str:
            s = v.rstrip('\"\'')
            s = s.lstrip('\"\'')
            return s
        return v

    def format_for_python(self, v):

        if self.type == "int":
            return str(v)
        elif self.type == "str":
            return f'"{v}"'
        elif self.type == "float":
            return str(v)

    def input_var(self, var_name_in_consuming_task):
        return InputVar(self, var_name_in_consuming_task)


class InputVar:

    def __init__(self, output_var, var_name_in_consuming_task):
        self.output_var = output_var
        self.var_name_in_consuming_task = var_name_in_consuming_task

    def write(self, string_value):
        return string_value


class Executor:

    def key(self):
        return None

class Local(Executor):

    def count_running_tasks(self):

        current_user = getpass.getuser()

        c = 0

        for it in psutil.process_iter(['pid', 'exe', 'username', 'environ']):

            if it.username() != current_user:
                continue
            try:

                if not it.exe().endswith("/bash"):
                    continue

                v = it.environ().get(LOCAL_PROCESS_IDENTIFIER_VAR)

                if v is not None: # and v.startswith("____"):
                    c += 1

            except AccessDenied:
                pass

        return c

    def kill_task(self, task_key):

        current_user = getpass.getuser()

        for it in psutil.process_iter(['pid', 'exe', 'username', 'environ']):

            if it.username() != current_user:
                continue
            try:

                if not it.exe().endswith("/bash"):
                    continue

                v = it.environ().get(LOCAL_PROCESS_IDENTIFIER_VAR)

                if v is not None:
                    if v == f"____{task_key}":
                        it.kill()

            except AccessDenied:
                pass

        #pids = list(get_pids())
        #print(f"pids: {pids}")
        #for pid in pids:
        #    os.kill(pid. signal.SIGKILL)

    def __init__(self, before_execute_bash):
        self.before_execute_bash = before_execute_bash
        self.cpu_count = len(psutil.Process().cpu_affinity())

    def has_cpu_capacity_to_launch(self):

        return self.count_running_tasks() <= self.cpu_count

    fail_silently_for_test = False

    """
        experimental, for scenario:
        + launcher runs in a LXC container
        + launcher runs tasks with singularity,launch on the physical host of the LXC container  
    """
    def __command_on_sibling_container(self, task, user, back_ground):
        return [
            f"ssh {user}@127.0.0.1 'nohup {task.v_abs_script_file()} " +
            f"  >{task.v_abs_out_log()} 2>{task.v_abs_err_log()} {back_ground}'"
        ]

    def execute(self, task, touch_pid_file_func, wait_for_completion=False, fail_silently=False):
        b4_command = ""
        if self.before_execute_bash is not None:
            b4_command = f"{self.before_execute_bash} &&"

        back_ground = "&"

        if wait_for_completion:
            back_ground = ""

        with subprocess.Popen(
                [f"nohup bash -c '{b4_command} . {task.v_abs_script_file()} " +
                 f"  >{task.v_abs_out_log()} 2>{task.v_abs_err_log()}' {back_ground}"],
                shell=True,
                text=True,
                env={
                    **task.extra_env,
                    **dict([
                        (LOCAL_PROCESS_IDENTIFIER_VAR, f"____{task.key}")
                    ]),
                    **os.environ
                },
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
        ) as p:

            p.wait()

            if p.returncode != 0:

                # If we were in async mode, we wouldn't see the error...
                if wait_for_completion:
                    if fail_silently:
                        return

                    if Local.fail_silently_for_test:
                        return

                    task_state = task.get_state()
                    err_if_failed = task_state.tail_err_if_failed(5)

                    if err_if_failed is not None:
                        raise Exception(
                            f"task {task.key} failed \n{err_if_failed}\n further details in {task.v_abs_err_log()}"
                        )
                    else:
                        return

                raise Exception(
                    f"the command for {task} returned non zero result, " +
                    f"see logs at {task.v_abs_out_log()} and {task.v_abs_err_log()}.\n",
                    f"invocation script: {task.v_abs_script_file()}"
                )

            touch_pid_file_func(p.pid)

SSH_TIMEOUT = 120

logger_ssh = logging.getLogger(f"{__name__}.ssh")


class SSHClientHolder:
    def __init__(self):
        from paramiko import SSHClient, AutoAddPolicy
        self.ssh_client = SSHClient()
        self.ssh_client.set_missing_host_key_policy(AutoAddPolicy)
        self._remote_overrides_uploaded = False

    def is_remote_overrides_uploaded(self):
        return self._remote_overrides_uploaded

    def set_remote_overrides_uploaded(self):
        self._remote_overrides_uploaded = True


class RemoteSSH(Executor):

    def __init__(
        self, ssh_username, ssh_host, remote_base_dir, key_filename, before_execute_bash, ssh_connection_spec, conf
    ):
        self.thread_local_ssh_client = threading.local()
        self.before_execute_bash = before_execute_bash
        self.remote_base_dir = remote_base_dir
        self.ssh_host = ssh_host
        self.ssh_username = ssh_username
        self.key_filename = key_filename
        self._key = ssh_connection_spec
        self.dependent_files = []
        self.rsync_containers = True
        self.slurm = None
        self.conf = conf

    def __str__(self):

        remote_pipeline_code_dir = self.conf.remote_pipeline_code_dir() or ""

        return f"{self.ssh_username}@{self.ssh_host}:{self.remote_base_dir}:{remote_pipeline_code_dir}"

    def is_remote_overrides_uploaded(self):
        return self.thread_local_ssh_client.h.is_remote_overrides_uploaded()

    def set_remote_overrides_uploaded(self):
        return self.thread_local_ssh_client.h.set_remote_overrides_uploaded()

    def ensure_connected(self):

        if not hasattr(self.thread_local_ssh_client, 'h'):
            from paramiko import SSHClient, AutoAddPolicy

            self.thread_local_ssh_client.h = SSHClientHolder()
            connection_was_initialized = False
        else:
            connection_was_initialized = True

        try:
            transport = self.ssh_client().get_transport()

            if transport is None:
                logger_ssh.debug("transport None, connection_was_initialized: %s", connection_was_initialized)
                self.connect()
                transport = self.ssh_client().get_transport()

            transport.send_ignore()
        except EOFError as e:
            logger_ssh.warning("ssh connection dead, will reconnect %s", e)
            self.connect()

    def ssh_client(self):
        return self.thread_local_ssh_client.h.ssh_client

    def add_dependent_file(self, file):
        if file not in self.dependent_files:
            self.dependent_files.append(file)

    def add_dependent_dir(self, dir):
        d = f"{dir}/"
        if d not in self.dependent_files:
            self.dependent_files.append(d)

    def key(self):
        return self._key

    def connect(self):
        try:
            with perf_logger_timer("RemoteSSH.connect") as t:
                self.ssh_client().connect(
                    self.ssh_host,
                    username=self.ssh_username,
                    key_filename=os.path.expanduser(self.key_filename)
                        if self.key_filename.startswith("~")
                        else self.key_filename,
                    timeout=SSH_TIMEOUT
                )
        except Exception as ex:
            logger_ssh.error(f"ssh connect failed: {self}")
            raise ex


    def invoke_remote(self, cmd, bash_error_ok=False):
        logger_ssh.debug("will invoke '%s' at %s", cmd, self.ssh_host)

        with perf_logger_timer("RemoteSSH.invoke_remote", cmd) as t:
            stdin, stdout, stderr = self.ssh_client().exec_command(cmd, timeout=SSH_TIMEOUT)

            stdout_txt = stdout.read().decode("utf-8")

        if not bash_error_ok and stdout.channel.recv_exit_status() != 0:
            stderr = stderr.read().decode("utf-8")
            raise Exception(f"remote call failed '{cmd}'\n{stderr}\non {self}")

        logger_ssh.debug(f"invocation of '%s' at {self} returned '%s'", cmd, stdout_txt)

        return stdout_txt

    def fetch_remote_task_states(self, pipeline):
        with perf_logger_timer("RemoteSSH.fetch_remote_task_states") as t:
            remote_pid_basename = os.path.basename(pipeline.pipeline_instance_dir)

            self.ensure_connected()

            cmd = f"find {self.remote_base_dir}/{remote_pid_basename}/.drypipe/*/state.* 2>/dev/null || true"
            stdout = self.invoke_remote(cmd)

            return [
                TaskState(f)
                for f in stdout.strip().split("\n")
                if f != ""
            ]

    def fetch_logs_and_history(self, task):

        self.ensure_connected()

        remote_pid_basename = os.path.basename(task.pipeline_instance_dir)

        #Not called because too slow !
        def fetch_remote_state():
            cmd = f"find {self.remote_base_dir}/{remote_pid_basename}/.drypipe/{task.key}/state.* 2>/dev/null"

            stdout = self.invoke_remote(cmd)

            return TaskState(stdout.strip())

        sftp = self.ssh_client().open_sftp()

        def file_content_and_last_mod_time(p):
            f = os.path.join(self.remote_base_dir, remote_pid_basename, p)
            s = sftp.stat(f)
            with sftp.open(f) as _f:
                return [_f.read(), s.st_mtime]

        f_out, t_out, \
        f_err, t_err, \
        f_history_file, t_history_file = flatten(map(file_content_and_last_mod_time, [
            task.out_log(),
            task.err_log(),
            task.history_file()
        ]))

        last_activity_time = max([t_err, t_out, t_history_file])

        return f_out.decode("utf-8"), f_err.decode("utf-8"), f_history_file.decode("utf-8"), last_activity_time

    def _remote_control_dir(self, task):
        return os.path.join(
            self.remote_base_dir,
            os.path.basename(task.pipeline_instance_dir),
            ".drypipe",
            task.key
        )

    def _file_in_control_dir(self, task, file):
        return os.path.join(
            self._remote_control_dir(task),
            file
        )

    def clear_remote_task_state(self, task):
        state_file = self._file_in_control_dir(task, "state.*")
        self.ensure_connected()
        self.invoke_remote(f"rm {state_file}", bash_error_ok=True)

    def kill_slurm_task(self, task):

        slurm_job_id_file = self._file_in_control_dir(task, "slurm_job_id")
        self.ensure_connected()

        sftp = self.ssh_client().open_sftp()

        with sftp.open(slurm_job_id_file) as _f:
            slurm_job_id = _f.read().strip().decode("utf-8")
            self.invoke_remote(f"scancel {slurm_job_id}")

    def delete_remote_state_file(self, file):

        self.ensure_connected()

        self.invoke_remote(f"rm {file}")

    def _rsync_with_args_and_remote_dir(self):

        ssh_args = f"-e 'ssh -q -i {self.key_filename} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'"

        timeout = f"--timeout={60 * 2}"

        return (
            f"rsync {ssh_args} {timeout}",
            f"{self.ssh_username}@{self.ssh_host}:{self.remote_base_dir}"
        )

    def _launch_command(self, command, exception_func=lambda stderr_text: stderr_text):

        with subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            text=True
        ) as p:
            p.wait()

            if p.returncode != 0:
                raise exception_func(p.stderr.read().strip())

    def do_rsync_container(self, image_path):

        rsync_call, remote_dir = self._rsync_with_args_and_remote_dir()

        bn = os.path.basename(image_path)
        dn = os.path.dirname(image_path)

        self.ensure_connected()

        self.invoke_remote(f"mkdir -p {self.remote_base_dir}/pipeline_code_dir")

        cmd = f"{rsync_call} -az --partial {image_path} {remote_dir}/pipeline_code_dir"

        self._launch_command(
            cmd,
            lambda err: Exception(f"uploading container {bn} failed:\n{cmd}\n{err}")
        )

    def rsync_remote_code_dir_if_applies(self, task_state, task):

        remote_pipeline_code_dir = task.remote_pipeline_code_dir()

        if remote_pipeline_code_dir is None:
            return

        with perf_logger_timer("RemoteSSH.rsync_remote_code_dir_if_applies") as t:

            rsync_call, remote_dir = self._rsync_with_args_and_remote_dir()

            self.ensure_connected()

            self.invoke_remote(f"mkdir -p {remote_pipeline_code_dir}")

            cmd = f"{rsync_call} -az --partial " + \
                  f"{task.v_abs_pipeline_code_dir()}/ {self.ssh_username}@{self.ssh_host}:{remote_pipeline_code_dir}"

            self._launch_command(
                cmd,
                lambda err: Exception(f"rsync of remote_pipeline_code_dir failed:\n{cmd}\n{err}")
            )

    def upload_task_inputs(self, task_state, task):
        with perf_logger_timer("RemoteSSH.upload_task_inputs") as t:

            task_control_dir = task_state.control_dir()

            rsync_call, remote_dir = self._rsync_with_args_and_remote_dir()
            pipeline_instance_dir = os.path.dirname(os.path.dirname(task_control_dir))

            remote_pid_basename = os.path.basename(pipeline_instance_dir)

            self.ensure_connected()

            self.invoke_remote(f"mkdir -p {self.remote_base_dir}/{remote_pid_basename}")

            cmd = f"{rsync_call} -aRz --partial --recursive --files-from={task_control_dir}/local-deps.txt " + \
                  f"{pipeline_instance_dir} {remote_dir}/{remote_pid_basename}"

            self._launch_command(
                cmd,
                lambda err: Exception(f"uploading of task inputs {task_control_dir} failed:\n{cmd}\n{err}")
            )

            # NO LONGER SUPPORTED
            # if self.rsync_containers and task.container is not None:
            #    self.do_rsync_container(task.container.image_path)

    def download_task_results(self, task_state):
        with perf_logger_timer("RemoteSSH.download_task_results") as t:
            task_control_dir = task_state.control_dir()

            rsync_call, remote_dir = self._rsync_with_args_and_remote_dir()
            pipeline_instance_dir = os.path.dirname(os.path.dirname(task_control_dir))
            remote_pid_basename = os.path.basename(pipeline_instance_dir)

            cmd = f"{rsync_call} --dirs -aR --partial --files-from={task_control_dir}/remote-outputs.txt " + \
                  f"{remote_dir}/{remote_pid_basename} {pipeline_instance_dir}"

            self._launch_command(
                cmd,
                lambda err: Exception(
                    f"downloading of task results {task_control_dir} from {self.ssh_host} failed:\n{cmd}\n{err}"
                )
            )

    def upload_overrides(self, pipeline):

        #if self.is_remote_overrides_uploaded():
        #    return

        pipeline_instance_dir = pipeline.pipeline_instance_dir
        remote_pid_basename = os.path.basename(pipeline_instance_dir)

        overrides_file_for_host = os.path.join(
            pipeline._work_dir,
            f"overrides-{self.ssh_host}.sh"
        )

        remote_pipeline_code_dir = self.conf.remote_pipeline_code_dir()
        remote_containers_dir = self.conf.vars.get("containers_dir")

        def gen_remote_overrides():
            yield "__pipeline_code_dir", remote_pipeline_code_dir
            yield "__containers_dir", remote_containers_dir

        remote_overrides = [
            f"export {k}={v}"
            for k, v in gen_remote_overrides()
            if v is not None
        ]

        if len(remote_overrides) == 0:
            return

        with open(overrides_file_for_host, "w") as _f:
            _f.write(f"{bash_shebang()}\n\n")
            _f.writelines(remote_overrides)
            _f.write("\n")

        #if not os.path.exists(overrides_file_for_host):
        #    return

        with perf_logger_timer("RemoteSSH.upload_overrides") as t:

            r_work_dir = os.path.join(self.remote_base_dir, remote_pid_basename, ".drypipe")
            r_override_file = os.path.join(
                r_work_dir,
                "overrides.sh"
            )

            self.ensure_connected()
            self.invoke_remote(f"mkdir -p {r_work_dir}")
            sftp = self.ssh_client().open_sftp()
            sftp.put(overrides_file_for_host, r_override_file)

            #self.set_remote_overrides_uploaded()


    """
        Fetches log lines and history.txt for all tasks, done once every janitor run, instead of before each task
    """
    def fetch_new_log_lines(self, pipeline_instance_dir):
        with perf_logger_timer("RemoteSSH.fetch_new_log_lines") as t:

            rsync_call, remote_dir = self._rsync_with_args_and_remote_dir()
            remote_pid_basename = os.path.basename(pipeline_instance_dir)
            dst = os.path.dirname(pipeline_instance_dir)

            cmd = " ".join([
                f" {rsync_call} -r --partial --append",
                " --filter='+ .drypipe/*/*.log'",
                " --filter='+ .drypipe/*/history.tsv'",
                " --filter='- .drypipe/*/*'",
                " --filter='- publish'",
                " --filter='- *.sh'",
                f" {remote_dir}/{remote_pid_basename} {dst}"
            ])

            logger_ssh.debug("will rsync new log lines \n%s", cmd)

            self._launch_command(
                cmd,
                lambda err: Exception(
                    f"fetching logs failed:\n{cmd}\n{err}")
            )

    def execute(self, task, touch_pid_file_func, wait_for_completion=False, fail_silently=False):
        with perf_logger_timer("RemoteSSH.execute") as t:
            b4_command = ""
            if self.before_execute_bash is not None:
                b4_command = f"{self.before_execute_bash} &&"

            remote_pid_basename = os.path.basename(task.pipeline_instance_dir)

            def remote_base_dir(p):
                return os.path.join(self.remote_base_dir, remote_pid_basename, p)

            r_script, r_sbatch_script, r_out, r_err, r_work_dir, r_control_dir = map(remote_base_dir, [
                task.script_file(),
                task.sbatch_launch_script(),
                task.out_log(),
                task.err_log(),
                task.work_dir(),
                task.control_dir()
            ])

            self.ensure_connected()

            self.invoke_remote(f"mkdir -p {r_work_dir}")
            self.invoke_remote(f"mkdir -p {r_control_dir}/out_sigs")

            if task.scratch_dir:
                self.invoke_remote(f"mkdir -p {remote_base_dir(task.scratch_dir())}")

            self.invoke_remote(f"rm {r_control_dir}/state.*", bash_error_ok=True)

            self.invoke_remote(f"touch {r_control_dir}/state.launched.0.0.1")

            if self.slurm is None:

                ampersand_or_not = "&"

                if wait_for_completion:
                    ampersand_or_not = ""

                cmd = f"nohup bash -c '{b4_command} . {r_script}  >{r_out} 2>{r_err}' {ampersand_or_not}"
            else:
                if wait_for_completion:
                    cmd = f"bash -c 'export SBATCH_WAIT=True && {r_sbatch_script}'"
                else:
                    cmd = r_sbatch_script

            self.invoke_remote(cmd)


class Slurm(Executor):

    def __init__(self, account, sbatch_options=[]):

        if account is None:
            raise Exception(f"account account can't be None")

        self.account = account
        self.sbatch_options = sbatch_options

    def execute(self, task, touch_pid_file_func, wait_for_completion=False, fail_silently=False):

        env = {
            **os.environ
        }

        if wait_for_completion:
            cmd = [f"bash -c 'export SBATCH_WAIT=True && {task.v_abs_sbatch_launch_script()}'"]
        else:
            cmd = [task.v_abs_sbatch_launch_script()]

        with subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            env=env,
            text=True
        ) as p:
            p.wait()

            if p.returncode != 0:
                err = p.stderr.read().strip()
                raise Exception(f"call to sbatch failed: {cmd}\n {err}")


class DynamicTaskConf:

    def __init__(
            self,
            specs=None,
            executer_type=None,
            ssh_specs=None,
            slurm_account=None,
            sbatch_options=[],
            container=None,
            containers_dir=None,
            command_before_launch_container=None,
            remote_pipeline_code_dir=None
    ):

        if specs is None:
            if executer_type is None:
                raise Exception("executer_type can't be None when specs is None")
            #if ssh_specs is None:
            #    raise Exception("ssp_specs can't be None when specs is None")

            if executer_type not in ["slurm", "process"]:
                raise Exception(f"invalid executer_type: {executer_type}")

            if executer_type == "slurm":
                if slurm_account is None:
                    raise Exception("slurm_account must be specified when executer_type is slurm")
                if slurm_account == "":
                    raise Exception("slurm_account can't be ''")

            self.executer_type = executer_type
            self.ssh_specs = ssh_specs
            varz = {
                "slurm_account": slurm_account,
                "sbatch_options": sbatch_options,
                "containers_dir": containers_dir,
                "container": container,
                "command_before_launch_container": command_before_launch_container,
                "remote_pipeline_code_dir": remote_pipeline_code_dir
            }

            self.vars = dict([
                (k, v) for k, v in varz.items() if v is not None
            ])

        else:
            if specs.startswith("$"):
                specs_value = os.environ.get(specs[1:])
                if specs_value is None:
                    raise Exception(f"dynamic conf refers to unset env variable {specs_value}")
            else:
                specs_value = specs

            if not re.match("(process|slurm)://.*", specs_value):
                raise Exception(f"invalid executer spec: {specs_value}\nmust begin with : (process|slurm)://")

            self.executer_type, ssh_specs_and_vars = specs_value.split("://")

            s = ssh_specs_and_vars.split("?")

            ssh_specs, varz = [s[0], None] if len(s) == 1 else s

            self.ssh_specs = None if ssh_specs == "" else ssh_specs

            def enum_args():
                for arg in varz.split("&"):
                    first_eq_idx = arg.find("=")
                    key = arg[0 : first_eq_idx]
                    val = arg[first_eq_idx + 1:]
                    if key == "sbatch_options":
                        val = val.split(",")
                    yield key, val

            self.vars = {} if varz is None else dict(list(enum_args()))

    def remote_pipeline_code_dir(self):
        return self.vars.get("remote_pipeline_code_dir")

    def override_executer(self, dsl, executer):

        remote_pipeline_code_dir = self.remote_pipeline_code_dir()

        if self.executer_type == "process":
            if self.ssh_specs is None:
                return dsl.local()
            else:
                r = dsl.remote_ssh(self.ssh_specs, conf=self, extra_key=f"rcd:{remote_pipeline_code_dir}")
                #remote_base_dir

                return r

        if self.executer_type == "slurm":
            account = self.vars.get("slurm_account")
            sbatch_options = self.vars.get("sbatch_options")
            slurm = dsl.slurm(account, sbatch_options)
            if self.ssh_specs is None:
                return slurm
            else:
                executer = dsl.remote_ssh(self.ssh_specs, conf=self, extra_key=f"rcd:{remote_pipeline_code_dir}")
                executer.slurm = slurm

        return executer

    def override_container(self, dsl, container):

        if "container" in self.vars:
            return dsl.singularity(
                self.vars["container"],
                command_before_launch_container=self.vars.get("command_before_launch_container")
            )

        return container


class Container:
    pass

class Singularity(Container):

    def __init__(self, image_path, binds, command_before_launch_container, singularity_args):

        if image_path is None or image_path == "":
            raise ValidationError(f"bad value for Singularity(image_path='{image_path}'")

        if os.path.splitext(image_path)[1] != ".sif":
            raise ValidationError(f"Singularity(image_path='{image_path}' does not appear to be a container image, doesn't end by .sif")

        #TODO rename to: image_file
        self.image_path = image_path
        self.binds = binds
        self.command_before_launch_container = command_before_launch_container
        self.singularity_args = singularity_args

    def prefix_env_var(self, name):
        return name


class IndeterminateFile:

    def __init__(self, file_path, manage_signature):
        self.file_path = file_path.strip()
        self.manage_signature = manage_signature

    def produced_file(self, var_name, producing_task):
        return ProducedFile(self.file_path, var_name, self.manage_signature, producing_task)

    def pre_existing_file(self):
        return PreExistingFile(self.file_path, self.manage_signature)


class FileSet:

    def __init__(self, glob_pattern):
        self.glob_pattern = glob_pattern

    def out_file_set(self, producing_task, name_in_producing_task):
        return OutFileSet(self, producing_task, name_in_producing_task)


class OutFileSet:

    def __init__(self, file_set, producing_task, name_in_producing_task):
        self.file_set = file_set
        self.producing_task = producing_task
        self.name_in_producing_task = name_in_producing_task

    def iterate(self):

        producing_task_work_dir = self.producing_task.v_abs_work_dir()

        pattern = os.path.join(producing_task_work_dir, self.file_set.glob_pattern)

        def make_path_relative_to_pipeline_instance_dir(p):
            return os.path.relpath(p, producing_task_work_dir)

        #if self.producing_task.pipeline.fake_splits:
        #    yield ProducedFile(pattern, self.name_in_producing_task, True, self.producing_task, is_dummy=True)
        #else:
        for f in glob.glob(pattern):
            f = make_path_relative_to_pipeline_instance_dir(f)
            yield ProducedFile(f, self.name_in_producing_task, True, self.producing_task)


class TaskMatcher:

    def __init__(self, task_keys_glob_pattern):
        self.task_keys_glob_pattern = task_keys_glob_pattern


class PreExistingFile:

    def __init__(self, file_path, manage_signature=False):

        if file_path is None or file_path == "" or file_path == ".":
            raise ValidationError(f"invalid file {file_path}")

        self.file_path = file_path
        self.manage_signature = manage_signature

    def to_hash(self):
        return self.file_path

    def value(self):
        return self.file_path

    def absolute_path(self, task):

        if os.path.isabs(self.file_path):
            return self.file_path

        return self.file_path


class ProducedFile:

    def __init__(self, file_path, var_name, manage_signature, producing_task, is_dummy=False):

        if file_path is None or file_path == "" or file_path == ".":
            raise ValidationError(f"invalid file {file_path}")

        if var_name is None or var_name == "":
            raise ValidationError(f"can't be {var_name}")

        self.var_name = var_name
        self.file_path = file_path
        self.producing_task = producing_task
        self.manage_signature = manage_signature
        self.is_dummy = is_dummy

    def to_hash(self):
        return self.file_path

    def value(self):
        return self.file_path

    def absolute_path(self, task):

        if os.path.isabs(self.file_path):
            return self.file_path

        return os.path.join(task.work_dir(), self.file_path)

    def input_file(self, var_name_in_consuming_task):
        return InputFile(self, var_name_in_consuming_task)


class InputFile:

    def __init__(self, produced_file, var_name_in_consuming_task):
        self.produced_file = produced_file
        self.var_name_in_consuming_task = var_name_in_consuming_task

def env_from_sourcing(env_file):

    p = os.path.abspath(sys.executable)

    dump_with_python_script = f'{p} -c "import os, json; print(json.dumps(dict(os.environ)))"'

    with subprocess.Popen(
            ['/bin/bash', '-c', f". {env_file} && {dump_with_python_script}"],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
    ) as pipe:
        pipe.wait()
        err = pipe.stderr.read()
        out = pipe.stdout.read()
        if pipe.returncode != 0:
            raise Exception(f"Failed sourcing env file: {env_file}\n{err}")
        return json.loads(out)


class PythonTask:

    def __init__(self, function_name, signature, module, func):
        self.function_name = function_name
        self.signature = signature
        self.module = module
        self.func = func

    def python_file(self):
        return self.module.__file__

    def k(self):
        return f"{self.python_file()}:${self.function_name}"


class Wait:

    def __init__(self, tasks):
        self.tasks = tasks

    def is_ready(self):
        for t in self.tasks:
            if not t.has_completed():
                return False

        return True

class TaskProps:

    def __init__(self, task, props):
        self.props = props
        self.task = task

    def __getattr__(self, name):
        if name not in self.props:
            raise ValidationError(f"{self.task} has no prop {name}")
        return self.props.get(name)


def flatten(the_list):
    return [item for sublist in the_list for item in sublist]


class MessageUtils:

    @staticmethod
    def safe_utf8(s):
        try:
            return s.decode("utf8")
        except UnicodeDecodeError as ex:
            return str(s)

