import glob
import inspect
import json
import os
import re
import subprocess
import sys
import textwrap
from itertools import groupby

import traceback

from dry_pipe.internals import \
    Executor, Container, Local, PreExistingFile, IndeterminateFile, ProducedFile, \
    Slurm, IncompleteVar, Val, Singularity, OutputVar, \
    ValidationError, env_from_sourcing, FileSet, TaskMatcher, PythonTask, RemoteSSH, Wait, DynamicTaskConf
from dry_pipe.pipeline import Pipeline

from dry_pipe.task import Task, TaskStep



class DryPipe:

    @staticmethod
    def dynamic_conf(**kwargs):
        return DynamicTaskConf(**kwargs)

    @staticmethod
    def dsl(pipeline_code_dir, pipeline_instance_dir=None):
        return DryPipeDsl(pipeline_code_dir, pipeline_instance_dir)

    annotated_python_task_by_name = {}

    @staticmethod
    def python_task(func):
        t = PythonTask(func.__name__, inspect.signature(func), inspect.getmodule(func), func)
        DryPipe.annotated_python_task_by_name[t.k()] = t
        return t

    """
        Will execute any func annotated by this dsl instance
        the python args must be call_python_task --env-file=<>
    """

    @staticmethod
    def python_task_cli(func_name, env_file=None):

        env = os.environ

        if env_file is not None:
            # or if pipeline provided --task=key
            env = env_from_sourcing(env_file)

        func_to_call = None
        for f in DryPipe.annotated_python_task_by_name.values():
            if f.function_name == func_name:
                func_to_call = f
                break

        if func_to_call is None:
            raise Exception(
                f"function {func_name} not found, or not callable.\n A function must exist and be" +
                "annotated with @DryPipe.python_task to be callable as a DryPipe task."
            )

        #TODO: Validate missing args
        args = [
            env.get(k)
            for k, v in func_to_call.signature.parameters.items()
        ]

        out_vars = None

        try:
            out_vars = func_to_call.func(* args)
        except Exception as ex:
            traceback.print_exc()
            exit(1)

        # TODO: complain if missing task __X vars

        control_dir = env["__control_dir"]

        try:
            if out_vars is not None:
                with open(env["__output_var_file"], "a+") as f:
                    for k, v in out_vars.items():
                        try:
                            v = json.dumps(v)
                        except TypeError as ex0:
                            print(
                                f"task call {control_dir}.{func_name} returned var {k} of unsupported type: {type(v)}" +
                                " only primitive types are supported.",
                                file=sys.stderr
                            )
                            exit(1)
                        f.write(f"{k}={v}\n")

            for pid_file in glob.glob(os.path.join(control_dir, "*.pid")):
                os.remove(pid_file)

        except Exception as ex:
            traceback.print_exc()
            exit(1)

    @staticmethod
    def create_pipeline(generator_of_tasks, dsl=None):

        if dsl is not None:
            def g():
                return generator_of_tasks(dsl)
            l_generator_of_tasks = g
        else:
            l_generator_of_tasks = generator_of_tasks


        def gen_and_validate_tasks(pipeline):
            from dry_pipe.task import Task

            try:
                iter(l_generator_of_tasks())
            except TypeError:
                raise ValidationError(f"function {l_generator_of_tasks} must return an iterable of Task (ex, via yield)")

            def check_yielded_task(task_or_function):

                if isinstance(task_or_function, TaskBuilder):
                    raise ValidationError(
                        f" task(key='{task_or_function.key}') is incomplete, you should probably call .calls(...) on it.")
                elif callable(task_or_function):
                    return None, task_or_function
                elif isinstance(task_or_function, Task):
                    task_or_function.pipeline = pipeline
                    return task_or_function, None
                else:
                    raise ValidationError(
                        f"iterator has yielded an invalid type: {type(task_or_function)}: '{task_or_function}'"
                    )

            def _handle_task_group():

                for wait_maybe in l_generator_of_tasks():
                    if not isinstance(wait_maybe, Wait):
                        yield wait_maybe
                    else:

                        for t0 in wait_maybe.tasks:
                            yield t0
                            yield lambda: t0.has_completed()

                        if not wait_maybe.is_ready():
                            break

            tasks_and_change_tracking_functions = [
                check_yielded_task(t) for t in _handle_task_group()
            ]

            tasks = [
                task
                for task, func in tasks_and_change_tracking_functions
                if task is not None
            ]

            change_tracking_functions = [
                func
                for task, func in tasks_and_change_tracking_functions
                if func is not None
            ]

            for t in tasks:
                if not isinstance(t, Task):
                    raise ValidationError(f"iterator has yielded an invalid type: {type(t)}")

            dup_keys = [
                k for k, task_group in groupby(tasks, lambda t: t.key) if len(list(task_group)) > 1
            ]

            if len(tasks) == 0:
                f = os.path.abspath(inspect.getmodule(generator_of_tasks).__file__)
                msg = f"pipeline {generator_of_tasks.__name__} defined in {f} yielded zero tasks"
                raise Exception(msg)

            if len(dup_keys) > 0:
                raise ValidationError(f"duplicate keys in definition: {dup_keys} value of task(key=) must be unique")

            def all_produced_files():
                for task in tasks:
                    for file in task.all_produced_files():
                        yield task, file

            def z(task, file):
                return file.absolute_path(task)

            for path, task_group in groupby(all_produced_files(), lambda t: z(*t)):
                task_group = list(task_group)
                if len(task_group) > 1:
                    task_group = ",".join(list(map(lambda t: str(t[0]), task_group)))
                    raise ValidationError(f"Tasks {task_group} have colliding output to file {path}."+
                                          " All files specified in Task(produces=) must be distinct")

            return tasks, change_tracking_functions

        return Pipeline(gen_and_validate_tasks)

    @staticmethod
    def dsl_for(task_generator_func):
        pipeline_code_dir = os.path.dirname(os.path.abspath(inspect.getmodule(task_generator_func).__file__))
        return DryPipe.dsl(pipeline_code_dir=pipeline_code_dir)



class DryPipeDsl:

    def with_defaults(self, pipeline_instance_dir=None, pipeline_code_dir=None, executer=None, extra_env=None, container=None, python_bin=None, conda_env=None):

        return DryPipeDsl(
            pipeline_code_dir or self.pipeline_code_dir,
            pipeline_instance_dir or self.pipeline_instance_dir,
            executer, extra_env, container, python_bin, conda_env
        )

    def wait_for(self, tasks):
        return Wait(tasks)

    def var(self, type=str, may_be_none=False):
        return IncompleteVar(type, may_be_none)

    def val(self, v):
        return Val(v)

    def slurm(self, account=None, sbatch_options=[]):
        return Slurm(account, sbatch_options)

    def local(self, before_execute_bash=None):
        return Local(before_execute_bash)

    remote_ssh_per_host_and_username = {}

    """
    format of ssh_connection_spec: 
    
        "{ssh_username}@{ssh_host}:{remote_base_dir}:{key_filename}"    
    """
    def remote_ssh(self, ssh_connection_spec, before_execute_bash=None, extra_key="", conf=None):

        ssh_username, rest = ssh_connection_spec.split("@")

        ssh_host, remote_base_dir, key_filename = rest.split(":")

        ssh_connection_spec = f"{ssh_connection_spec}|{extra_key}"

        instance = self.remote_ssh_per_host_and_username.get(ssh_connection_spec)

        if instance is not None:
            return instance

        instance = RemoteSSH(
            ssh_username, ssh_host, remote_base_dir, key_filename, before_execute_bash, ssh_connection_spec, conf
        )

        self.remote_ssh_per_host_and_username[ssh_connection_spec] = instance

        return instance

    def singularity(self, image_path, binds={}, command_before_launch_container=None, singularity_args=""):
        return Singularity(image_path, binds, command_before_launch_container, singularity_args)

    def file(self, name, manage_signature=None):

        if type(name) != str:
            raise ValidationError(f"invalid file name, must be a string {name}")

        return IndeterminateFile(name, manage_signature)

    def fileset(self, glob_pattern):

        return FileSet(glob_pattern)

    def matching_tasks(self, task_keys_glob_pattern):

        return TaskMatcher(task_keys_glob_pattern)

    def task(self,
             key,
             executer=None,
             container=None,
             extra_env={},
             dynamic_conf=None):

        executer = executer or self.executer

        container = container or self.container

        if key is None or key == "":
            raise Exception(f"invalid key given to task(...): '{key}'")

        if self.extra_env is not None:
            extra_env = {
                ** self.extra_env,
                ** extra_env
            }

        if dynamic_conf is not None:
            if isinstance(dynamic_conf, str):
                dynamic_conf = DynamicTaskConf(dynamic_conf)
            container = dynamic_conf.override_container(self, container)
            executer = dynamic_conf.override_executer(self, executer)

        self._check_container(container)
        self._check_executer(executer)

        return TaskBuilder(
            key=key,
            executer=executer,
            container=container,
            extra_env=extra_env,
            _produces={},
            _depends_on={},
            dsl=self,
            dynamic_conf=dynamic_conf
        )

    @staticmethod
    def _check_container(container):
        if container is not None and not isinstance(container, Singularity):
            raise ValidationError(
                f"invalid type {type(container)} for constructor arg Task(container=), needs DryPipe.Singularity"
            )

    @staticmethod
    def _check_executer(executer):
        if not isinstance(executer, Executor):
            raise ValidationError(f"invalid type {type(executer)} for constructor arg Task(executer=.")


    def __init__(self,
        pipeline_code_dir=None,
        pipeline_instance_dir=None,
        executer=None,
        extra_env={},
        container=None, python_bin=None, conda_env=None,
        containers_dir=None):

        if pipeline_code_dir is None or pipeline_code_dir == '':
            raise ValidationError("pipeline_code_dir can't be None or empty")

        if pipeline_instance_dir is None:
            pipeline_instance_dir = pipeline_code_dir

        if containers_dir is None:
            containers_dir = os.path.join(pipeline_code_dir, "containers")

        self.pipeline_instance_dir = os.path.abspath(pipeline_instance_dir)

        self.pipeline_code_dir = pipeline_code_dir

        self._check_container(container)

        self.python_bin = python_bin
        self.conda_env = conda_env

        if executer is None:
            executer = self.local()
        else:
            self._check_executer(executer)

        self.container = container
        self.executer = executer
        self.extra_env = extra_env

        self.annotated_python_task_by_name = {}


class TaskBuilder:

    def __init__(self, key=None, executer=None, container=None, _depends_on={}, _produces={},
                 extra_env={}, dsl=None, task_steps=[], dependent_scripts=[],
                 _upstream_task_completion_dependencies=None, _props=None, dynamic_conf=None):

        self.key = key
        self.dsl = dsl
        self.executer = executer
        self.container = container
        self._props = _props or {}
        self._depends_on = _depends_on
        self._upstream_task_completion_dependencies = _upstream_task_completion_dependencies or []
        self._produces = _produces
        self.task_steps = task_steps
        self.dependent_scripts = dependent_scripts
        self.extra_env = extra_env
        self.dynamic_conf = dynamic_conf

    def _deps_from_kwargs(self, kwargs):

        def deps():
            for k, v in kwargs.items():
                if isinstance(v, IndeterminateFile):
                    yield k, v
                elif isinstance(v, ProducedFile) or isinstance(v, Val) or isinstance(v, OutputVar):
                    yield k, v
                elif isinstance(v, TaskMatcher):
                    yield k, v
                else:
                    raise ValidationError(
                        f"depends_on can only take DryPipe.file() or depends_on(a_file=other_task.out.name_of_file()" +
                        f"task(key={self.key}) was given {type(v)}",
                        ValidationError.depends_on_has_invalid_kwarg_type
                    )
        return {
            ** self._depends_on,
            ** dict(deps())
        }

    def depends_on(self, *args, **kwargs):

        def upstream_task_completion_dependencies():
            for o in args:
                if isinstance(o, Task):
                    yield o
                else:
                    raise ValidationError(
                        f"{self}.depends_on(...) must be a list of key=value, or a task, whose completion is depended upon"
                    )

        return TaskBuilder(** {
            ** vars(self),
            ** {"_upstream_task_completion_dependencies": list(upstream_task_completion_dependencies())},
            ** {"_depends_on": {
                ** self._depends_on,
                ** dict(self._deps_from_kwargs(kwargs))
                }
            }
        })

    def props(self, **kwargs):
        return TaskBuilder(** {
            ** vars(self),
            ** {"_props": kwargs}
        })

    def depends_on_matching_tasks(self, key_match_expr: str, **kwargs):

        return TaskBuilder(** {
            ** vars(self),
            ** {"_depends_on_matching_tasks": self._deps_from_kwargs(kwargs)}
        })

    def produces(self, *args, **kwargs):

        if len(args) > 0:
            raise ValidationError(
                f"DryPipe.produces(...) can't take positional args, use the form produce(var_name=...)",
                ValidationError.produces_cant_take_positional_args
            )

        def outputs():
            for k, v in kwargs.items():
                if isinstance(v, IndeterminateFile):
                    yield k, v
                elif isinstance(v, IncompleteVar):
                    yield k, v
                elif isinstance(v, FileSet):
                    yield k, v
                else:
                    raise ValidationError(
                        f"produces takes only DryPipe.file or DryPipe.vars, ex:\n " +
                        "1:    task(...).produces(var_name=DryPipe.file('abc.tsv'))\n"
                        " 2:    task(...).produces(vars=DryPipe.vars(x=123,s='a'))",
                        ValidationError.produces_only_takes_files
                    )

        return TaskBuilder(** {
            ** vars(self),
            ** {"_produces": dict(list(outputs()))}
        })

    @staticmethod
    def is_remote_ssh_executer(ex):
        return isinstance(ex, RemoteSSH)

    def calls(self, *args, **kwargs):

        container = kwargs.get("container")
        executer = kwargs.get("executer")

        if self.is_remote_ssh_executer(executer):
            raise ValidationError(f"A RemoteSSH executer was given to {self}.calls(), it is only " +
                                  " supported at the task level, please pass as a task() argument")
        task_step = None

        if len(args) == 1:
            a = args[0]
            if type(a) == str:
                if a.endswith(".sh"):
                    task_step = TaskStep(
                        shell_script=a,
                        container=container,
                        executer=executer
                    )
                else:
                    script_text = textwrap.dedent(a)
                    if re.match("\\n(\\w*)#!/.*", script_text):

                        start_idx = script_text.find("#!")
                        script_text = script_text[start_idx:-1]

                        task_step = TaskStep(
                            shell_snippet=script_text,
                            container=container,
                            executer=executer
                       )
                    else:
                        raise ValidationError(
                            f"invalid arg to clause:\n ...calls({a})\nvalid arg is a script file (.sh suffix), " +
                            "or a code block with shebang (ex):\n" +
                            "#!/usr/bin/env bash"
                            "echo '...something...'"
                        )

            elif isinstance(a, PythonTask):
                task_step = TaskStep(
                    python_task=a,
                    python_bin=kwargs.get("python_bin") or self.dsl.python_bin or sys.executable,
                    conda_env=self.dsl.conda_env,
                    container=container,
                    executer=executer
                )

        if task_step is None:
            raise ValidationError(
                f"invalid args, task.calls(...) can take a sigle a single positional argument, was given: {args}",
                ValidationError.call_has_bad_arg
            )

        return TaskBuilder(** {
            ** vars(self),
            "task_steps": self.task_steps + [task_step]
        })

    def __call__(self):
        self.task_steps[-1].signs_output_files = True
        return Task(self)


def host_has_sbatch():

    with subprocess.Popen(
            ["which", "sbatch"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
    ) as p:

        p.wait()

        if p.returncode != 0:
            return False

        out = p.stdout.read().strip()

        if out == "":
            return False

        if out.endswith("/sbatch"):
            return True

        raise Exception(f"Funny return from which sbatch: {out}")
