import logging
import os
import pathlib
import shutil
import subprocess
from itertools import groupby

from dry_pipe.bash import BASH_SIGN_FILES_IF_NEWER, BASH_TASK_FUNCS_AND_TRAPS, BASH_SIGN_FILES, bash_shebang
from dry_pipe.internals import ValidationError
from dry_pipe.pipeline_state import PipelineState
from dry_pipe.task import Task

logger = logging.getLogger(__name__)

class TaskSet:

    def __init__(self, validated_tasks_gen, pipeline):

        self.pipeline = pipeline
        self.validated_tasks_gen = validated_tasks_gen
        self._tasks, self._change_tracking_functions = self.validated_tasks_gen(pipeline)
        self._tasks_signature = self._calculate_tasks_signature()

    def remote_executors(self):

        def gen():
            for task in self:
                if task.is_remote():
                    e = task.executer()
                    yield str(e), e

        return dict(gen()).values()

    def __iter__(self):
        for t in self._tasks:
            yield t

    def __len__(self):
        return len(self._tasks)

    def __getitem__(self, task_key):

        task = self.get(task_key)

        if task is not None:
            return task

        raise KeyError(f"pipeline has no task {task_key}")

    def dsl(self):
        return self._tasks[0].dsl

    def get(self, task_key):
        for task in self._tasks:
            if task.key == task_key:
                return task

    def _calculate_tasks_signature(self):
        return [
            f() for f in self._change_tracking_functions
        ]

    def regen_if_stale_else_self(self):
        sig = self._calculate_tasks_signature()

        if len(sig) != len(self._tasks_signature):
            raise ValidationError(
                "task generating function yielded different number of change tracking functions " +
                f" {len(sig)}" != f"{len(self._tasks_signature)}, the number of change tracking " +
                "functions must be constant"
            )

        if sig != self._tasks_signature:
            return TaskSet(self.validated_tasks_gen, self.pipeline)
        else:
            return self

    def __obsolete(self):

        def tasks_per_non_local_executer():
            for task in self._tasks:
                non_local_executor = task.non_local_executer()
                if non_local_executor is not None:
                    yield non_local_executor, task

        def executer_key(t):
            return t[0].key()

        self.tasks_per_non_local_executer = [
            (ex, list(tasks))
            for ex, tasks
            in groupby(sorted(tasks_per_non_local_executer(), key=executer_key), key=executer_key)
        ]


class Pipeline:

    def __init__(self, validated_tasks_gen):
        self.fake_splits = False
        self.tasks = TaskSet(validated_tasks_gen, self)
        self.dsl = self.tasks.dsl()
        self.pipeline_instance_dir = self.dsl.pipeline_instance_dir
        self._work_dir = os.path.join(self.pipeline_instance_dir, ".drypipe")
        self._publish_dir = os.path.join(self.pipeline_instance_dir, "publish")

    def init_work_dir_if_not_exists(self, extra_env=None, force=False):
        if not os.path.exists(self._work_dir) or force:
            self.ensure_work_dirs_initialized(extra_env)
            self.calc_pre_existing_files_signatures()
            self.get_state().touch()
        else:
            logger.debug("will skip work dir creation %s", self._work_dir)

    def create_sourced_env(self, extra_env, update_if_exists=False):

        ef = os.path.join(self._work_dir, "extra_env.sh")
        if not os.path.exists(ef) or update_if_exists:
            with open(ef, "w") as f:
                f.write(f"{bash_shebang()}\n\n")

                if extra_env is not None:
                    for k, v in extra_env.items():
                        f.write(f"export {k}={v}\n")

            os.chmod(ef, 0o764)

    def ensure_work_dirs_initialized(self, extra_env=None):

        pathlib.Path(self._publish_dir).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self._work_dir).mkdir(parents=True, exist_ok=True)

        self.create_sourced_env(extra_env)

        launcher_funcs = os.path.join(self._work_dir, "launcher-funcs.sh")

        if not os.path.exists(launcher_funcs):
            with open(launcher_funcs, "w") as f:
                f.write(f"{bash_shebang()}\n\n")
                f.write(BASH_TASK_FUNCS_AND_TRAPS)
                f.write(BASH_SIGN_FILES)

            os.chmod(launcher_funcs, 0o764)
            logger.debug("%s initialized", launcher_funcs)

        if not os.path.exists(self._recalc_hash_script()):
            with open(self._recalc_hash_script(), "w") as f:
                f.write(f"{bash_shebang()}\n\n")
                f.write(BASH_SIGN_FILES_IF_NEWER)
                f.write("\n__sign_files\n")

            os.chmod(self._recalc_hash_script(), 0o764)

            logger.debug("%s initialized", self._recalc_hash_script())

    def regen_tasks_if_stale(self):
        # TODO: get rid of mutation !
        self.tasks = self.tasks.regen_if_stale_else_self()

    def instance_dir_base_name(self):
        return os.path.basename(self.pipeline_instance_dir)

    def _recalc_hash_script(self):
        return os.path.join(self._work_dir, "recalc-output-file-hashes.sh")

    def annihilate_work_dirs(self):
        shutil.rmtree(self._publish_dir, ignore_errors=True)
        shutil.rmtree(self._work_dir, ignore_errors=True)

    def work_dir_exists(self):
        return os.path.exists(self._work_dir)

    def tasks_for_glob_expr(self, glob_expr):
        return [
            task
            for task in self.tasks
            if pathlib.PurePath(task.key).match(glob_expr)
        ]

    def tasks_for_key_prefix(self, key_prefix):
        for task in self.tasks:
            if task.key.startswith(key_prefix):
                yield task

    def clean(self):

        for task in self.tasks:
            task.clean()

    def clean_all(self):
        if os.path.exists(self._work_dir):
            shutil.rmtree(self._work_dir)

        if os.path.exists(self._publish_dir):
            shutil.rmtree(self._publish_dir)

    def get_state(self, create_if_not_exists=False):
        return PipelineState.from_pipeline_work_dir(self._work_dir, create_if_not_exists)

    def summarized_dependency_graph(self, fake_splits=True):

        def gen_deps():
            for task in self.tasks:
                for upstream_task, upstream_input_files, upstream_input_vars in task.upstream_deps_iterator():
                    yield [
                        Task.key_grouper(task.key),
                        [
                            k.produced_file.var_name
                            for k in upstream_input_files
                        ] + [
                            k.output_var.name
                            for k in upstream_input_vars
                        ],
                        Task.key_grouper(upstream_task.key)
                    ]

                for k, m in task.task_matchers.items():
                    for upstream_task in self.tasks:
                        if pathlib.PurePath(upstream_task.key).match(m.task_keys_glob_pattern):
                            yield [
                                Task.key_grouper(task.key),
                                [m.task_keys_glob_pattern],
                                Task.key_grouper(upstream_task.key)
                            ]

        def g(iterable, func):
            return groupby(sorted(iterable, key=func), key=func)

        def gen_tasks():
            for _t, tasks in g(self.tasks, lambda t: Task.key_grouper(t.key)):
                yield {
                    "key_group": _t,
                    "keys": list([t.key for t in tasks])
                }

        def gen_group_deps():
            for _d, deps in g(gen_deps(), lambda t: f"{t[0]}-{t[2]}"):
                yield next(deps)

        return {
            "taskGroups": list(gen_tasks()),
            "deps": list(gen_group_deps())
        }

    # TODO: avoid name clashes when pre existing files with the same names
    def calc_pre_existing_files_signatures(self, force_recalc=False):

        def name_file_paths_tuples():
            for task in self.tasks:
                for name, pre_existing_file in task.pre_existing_files.items():
                    p = task.abs_from_pipeline_instance_dir(pre_existing_file.absolute_path(task))

                    if not os.path.exists(p):
                        raise Exception(f"{task} depends on missing pre existing file '{name}'='{p}'")

                    yield p

        all_pre_existing_files = {
            f for f in name_file_paths_tuples()
        }

        sig_dir = os.path.join(self._work_dir, "in_sigs")

        if not os.path.exists(sig_dir):

            pathlib.Path(sig_dir).mkdir(parents=True, exist_ok=True)

            with subprocess.Popen(
                    f"{self._work_dir}/recalc-output-file-hashes.sh",
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    text=True,
                    env={
                        "__control_dir": self._work_dir,
                        "__sig_dir": "in_sigs",
                        "__file_list_to_sign": ",".join(all_pre_existing_files)
                    }
            ) as p:
                p.wait()
                err = p.stderr.read()
                if p.returncode != 0:
                    raise Exception(f"Failed signing pre existing files: {err}")

    """
    Recompute output signatures (out.sig) of all tasks, and identifies tasks who's inputs has changed by 
    
    creating a filed named "in.sig.changed" in the task's control_dir.
    """

    def recompute_signatures(self, force_recalc_of_preexisting_file_signatures=True):

        self.calc_pre_existing_files_signatures(force_recalc_of_preexisting_file_signatures)

        for task in self.tasks:
            task.recompute_output_singature(self._recalc_hash_script())

        total_changed = 0

        for task in self.tasks:
            if task.has_completed():

                previously_computed_input_signature = task.input_signature()

                up_to_date_input_signature, in_sig_file_writer = task.calc_input_signature()

                if previously_computed_input_signature != up_to_date_input_signature:
                    total_changed += 1
                    in_sig_file_writer(write_changed_flag=True)
                else:
                    task.clear_input_changed_flag()

        return total_changed


SLURM_SQUEUE_FORMAT_SPEC = "%A %L %j %l %T"


def parse_status_from_squeue_line(squeue_line):
    return squeue_line[4]


def call_squeue_for_job_id(job_id, parser=None):
    cmd = f"squeue -h -j {job_id} -o '{SLURM_SQUEUE_FORMAT_SPEC}'"

    with subprocess.Popen(
            cmd.split(" "),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
    ) as p:

        p.wait()

        if p.returncode != 0:
            raise Exception(f"call failed '{cmd}', return code: {p.returncode}\n{p.stderr}")

        out_line = p.stdout.read().strip()

        if len(out_line) == 0:
            return None
        elif len(out_line) > 1:
            raise Exception(f"squeue dared return more than one line when given {cmd}")

        line = map(lambda s: s.strip(), out_line[0].split(" "))

        if parser is not None:
            return parser(line)

        return line


def is_module_command_available():
    cmd = ["bash", "-c", "type -t module"]
    with subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
    ) as p:
        p.wait()
        if p.returncode != 0:
            raise Exception(f"Failed while checking if module available {cmd} ")

        out = p.stdout.read().strip()

        return out == "function"
