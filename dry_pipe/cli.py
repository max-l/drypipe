import inspect
import json
import pathlib
import re
import time
import os
from threading import Thread
import click
from rich.console import Console
from rich.live import Live
from rich.table import Table
from dry_pipe import DryPipe
from dry_pipe.janitors import Janitor
from dry_pipe.monitoring import PipelineMetricsTable, fetch_task_group_metrics, fetch_task_groups_stats
from dry_pipe.server import PipelineUIServer
from dry_pipe.task_state import NON_TERMINAL_STATES
from dry_pipe.websocket_server import WebsocketServer



@click.group()
@click.option('-v', '--verbose')
@click.pass_context
def cli_group(ctx, verbose):
    ctx.ensure_object(dict)
    ctx.obj['verbose'] = verbose
    ctx.obj["ctx"] = ctx


def _bad_entrypoint(ctx):

    if "entrypoint" in ctx.obj and ctx.obj["entrypoint"] == "run_task":
        click.echo("Can't launch a pipeline with cli.run_task(), use cli.run(pipeline=<your pipeline>)", err=True)
        return True

    return False

@click.command()
@click.pass_context
@click.option('--instance-dir', type=click.Path(), default=None)
def clean(ctx, instance_dir):
    if _bad_entrypoint(ctx):
        return
    pipeline_func = ctx.obj["pipeline_func"]
    pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)
    # TODO: prompt
    pipeline.clean()


def _pipeline_from_pipeline_func(pipeline_func, instance_dir):

    pipeline_func_sig = inspect.signature(pipeline_func)

    nargs = len(pipeline_func_sig.parameters)

    if nargs == 0:

        if instance_dir is not None:
            raise Exception(f"when specifying instance_dir, task generator must take a dry_pipe.DryPipe.dsl() argument")

        return DryPipe.create_pipeline(pipeline_func)
    elif nargs == 1:
        dsl = DryPipe.dsl_for(pipeline_func)
        if instance_dir is not None:
            dsl = dsl.with_defaults(pipeline_instance_dir=instance_dir)
        return DryPipe.create_pipeline(pipeline_func, dsl)
    else:
        m = inspect.getmodule(pipeline_func)
        raise Exception(f"function {pipeline_func.__name__} in {m.__file__} takes {nargs} arguments,\n"
                        f"it should take a instance created with dry_pipe.DryPipe.dsl()")


@click.command()
@click.pass_context
@click.option('--clean', is_flag=True)
@click.option('--instance-dir', type=click.Path(), default=None)
def prepare(ctx, clean, instance_dir):

    if _bad_entrypoint(ctx):
        return

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)

    if clean:
        pipeline.clean_all()

    pipeline.ensure_work_dirs_initialized()

    pipeline.get_state().touch()

    tasks_total = 0
    work_done = 0
    tasks_in_non_terminal_states = 0
    tasks_completed = 0

    for task in pipeline.tasks:

        tasks_total += 1

        task_state = task.get_state()

        if task_state is None:
            task.create_state_file_and_control_dir()
            work_done += 1
            task_state = task.get_state()

        if task_state.state_name in NON_TERMINAL_STATES:
            tasks_in_non_terminal_states += 1

        if task_state.is_waiting_for_deps():

            if not task.has_unsatisfied_deps():
                if task_state.is_prepared():
                    continue
                else:
                    task_state.transition_to_prepared(task)
                    task_state = task.get_state()
                    task_state.transition_to_queued(task)
                    task.prepare()
                    tasks_in_non_terminal_states += 1
                    work_done += 1

        elif task_state.is_completed():
            if task_state.action_if_exists() is None:
                tasks_completed += 1

    if tasks_total == tasks_completed:
        pipeline_state = pipeline.get_state()
        if not pipeline_state.is_completed():
            pipeline_state.transition_to_completed()




@click.command()
@click.pass_context
@click.option('--clean', is_flag=True)
@click.option('--single', type=click.STRING, default=None,
              help="launches a single task, specified by the task key, in foreground, ex.: launch --single=TASK_KEY")
@click.option('--web-mon', is_flag=True)
@click.option('--port', default=5000, help="port for --web-mon")
@click.option('--bind', default="0.0.0.0", help="bind address for --web-mon")
@click.option('--env', type=click.STRING, default=None)
@click.option('--instance-dir', type=click.Path(), default=None)
@click.option('--no-confirm', is_flag=True)
@click.option('--restart-failed', is_flag=True)
def launch(ctx, clean, single, web_mon, port, bind, env, instance_dir, no_confirm, restart_failed):

    if _bad_entrypoint(ctx):
        return

    confirm_func = ctx.obj.get("confirm_func")

    if not single and not no_confirm and confirm_func is not None and not confirm_func():
        click.echo("nothing done.")
        return

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)

    pipeline.ensure_work_dirs_initialized()

    pipeline.get_state().touch()

    if env is not None:
        env = dict([
            tuple(k_v.split(":"))
            for k_v in env.split(",")
        ])

    if clean:
        pipeline.clean()

    if single is not None:
        launch_single(pipeline, single)
        return

    for task in pipeline.tasks:
        #task.prepare()
        if restart_failed and task.get_state().is_failed():
            task.re_queue()

    janitor = Janitor(
        pipeline,
        min_sleep=0,
        max_sleep=5
    )

    janitor_thread = janitor.start(env)

    janitor.start_remote_janitors()

    def _cli_ui():

        with Live(refresh_per_second=1) as live:

            def do_update(_pipeline):
                while not janitor.is_shutdown():

                    live.update(_status_table(_pipeline))
                    time.sleep(2)

            t = Thread(target=lambda: do_update(pipeline))
            t.start()
            janitor_thread.join()
            janitor.do_shutdown()
            live.update(_status_table(pipeline))

    if web_mon:
        server = PipelineUIServer.init_single_pipeline_instance(pipeline.get_state())
        server.start()

        WebsocketServer.start(bind, port)
        # Stopping threads break CTRL+C, let the threads die with the process exit
        #server.stop()
        #janitor.do_shutdown()
    else:
        _cli_ui()

    os._exit(0)


def launch_single(pipeline, single):

    task = pipeline.tasks.get(single)

    if task is None:
        raise Exception(f"pipeline has no task with key '{single}'")

    task.re_queue()
    task.prepare()
    task_state = task.get_state()
    task_state.transition_to_launched(task, wait_for_completion=True)
    task_state = task.get_state()
    task_state.transition_to_completed(task)

    print(f"Task {single} completed.")


@click.command()
@click.pass_context
@click.option('--instance-dir', type=click.Path(), default=None)
@click.option('--single', type=click.STRING, default=None,
              help="apply to single task, specified by the task key, in foreground, ex.: launch --single=TASK_KEY")
def requeue(ctx, single, instance_dir):

    if _bad_entrypoint(ctx):
        return

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)

    if single is None:
        raise Exception(f"unimplemented.")

    requeue_single(pipeline, single)


def requeue_single(pipeline, single):

    task = pipeline.tasks.get(single)

    if task is None:
        raise Exception(f"pipeline has no task with key '{single}'")

    task.re_queue()
    task.prepare()

    print(f"Task {single} requeued")


@click.command()
@click.pass_context
@click.option('--env', type=click.STRING, default=None)
@click.option('--instances-dir', type=click.STRING)
def watch(ctx, env, instances_dir):

    if instances_dir is None:
        raise Exception(
            f"serve-multi requires mandatory option --instances-dir=<directory of pipeline instances>"
        )

    if not os.path.exists(instances_dir):
        pathlib.Path(instances_dir).mkdir(parents=True)

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline_code_dir = os.path.dirname(os.path.abspath(inspect.getmodule(pipeline_func).__file__))

    janitor = Janitor(
        pipeline_instances_dir=instances_dir,
        task_generator=pipeline_func,
        dsl=DryPipe.dsl(pipeline_code_dir=pipeline_code_dir),
    )

    thread = janitor.start()

    janitor.start_remote_janitors()

    thread.join()

    os._exit(0)


def _validate_task_generator_callable_with_single_dsl_arg_and_get_module_file(task_generator):

    a = inspect.signature(task_generator)

    f = os.path.dirname(os.path.abspath(inspect.getmodule(task_generator).__file__))

    if len(a.parameters) != 1:
        raise Exception(f"function given to cli.run() should take a singe DryPipe.dsl() as argument, and yield tasks" +
                        f" was given {len(a.parameters)} args")

    return f


@click.command()
@click.pass_context
@click.option('--instances-dir', type=click.STRING)
@click.option('--bind', default="0.0.0.0", help="bind address for --web-mon")
@click.option('--port', default=5000)
def serve_ui(ctx, instances_dir, bind, port):

    if instances_dir is None:
        raise Exception(
            f"serve-multi requires mandatory option --instances-dir=<directory of pipeline instances>"
        )

    if not os.path.exists(instances_dir):
        pathlib.Path(instances_dir).mkdir(parents=True)

    WebsocketServer.start(bind, port, instances_dir)

    os._exit(0)


@click.command()
@click.pass_context
@click.option('--bind', default="0.0.0.0", help="bind address for --web-mon")
@click.option('--port', default=5000)
def hub(ctx, bind, port):
    WebsocketServer.start(bind, port)


@click.command()
@click.pass_context
@click.argument('func-name', type=click.STRING)
@click.option('--env-file', type=click.Path(exists=True, dir_okay=False))
def call(ctx, func_name, env_file):

    DryPipe.python_task_cli(func_name, env_file)


@click.command()
@click.pass_context
@click.option('--downstream-of', type=click.STRING)
@click.option('--not-matching', type=click.STRING)
@click.option('--matching', type=click.STRING)
@click.option('--instance-dir', type=click.Path(), default=None)
def reset(ctx, downstream_of, not_matching, matching, instance_dir):

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)

    if matching is not None or not_matching is not None:

        if matching is not None and not_matching is not None:
            raise Exception("--matching and --not-matching can't be both specified")

        def do_reset(key):
            if matching is not None:
                return re.match(matching, key)
            return not re.match(not_matching, key)

        for task in pipeline.tasks:
            if do_reset(task.key):
                if click.confirm(f"reset {task.key} ?"):
                    task.clean()
                    print(f"{task.key} reset")

    elif downstream_of is not None:

        after_task_including = pipeline.tasks.get(downstream_of)

        if after_task_including is None:
            raise Exception(f"pipeline has no task with key '{downstream_of}'")

        def all_upstream_deps(task):
            yield task
            for upstream_task, _1, _2 in task.upstream_deps_iterator():
                for t in all_upstream_deps(upstream_task):
                    yield t

        raise Exception("not implemented")
    else:
        if click.confirm("reset all tasks ?"):
            print("boom")


def _status_table(pipeline):

    table = Table(show_header=True, header_style="bold magenta")

    header, body, footer = PipelineMetricsTable.summarized_table_from_task_group_metrics(
        fetch_task_group_metrics(pipeline)
    )

    for h in header:
        table.add_column(h)

    for row in body:

        row_cells = [
            str(v or "")
            for v in row
        ]

        table.add_row(*row_cells)

    table.add_row(*[
        str(v or "")
        for v in footer
    ])

    return table

@click.command()
@click.pass_context
@click.option('--instance-dir', type=click.Path(), default=None)
def stats(ctx, instance_dir):

    the_stats = None

    if instance_dir is not None:
        the_stats = fetch_task_groups_stats(instance_dir)
    else:
        pipeline_func = ctx.obj["pipeline_func"]
        pipeline = _pipeline_from_pipeline_func(pipeline_func, instance_dir)
        the_stats = fetch_task_groups_stats(pipeline.pipeline_instance_dir)

    for stat_row in the_stats:
        print("\t".join([str(v) for v in stat_row]))


@click.command()
@click.pass_context
def status(ctx):

    raise Exception(f"not implemented")

    if _bad_entrypoint(ctx):
        return

    pipeline_func = ctx.obj["pipeline_func"]

    pipeline = DryPipe.create_pipeline(pipeline_func)
    console = Console()
    console.print(_status_table(pipeline))


def _register_commands():
    cli_group.add_command(launch)
    cli_group.add_command(prepare)
    cli_group.add_command(call)
    cli_group.add_command(status)
    cli_group.add_command(watch)
    cli_group.add_command(serve_ui)
    cli_group.add_command(clean)
    cli_group.add_command(hub)
    cli_group.add_command(stats)
    cli_group.add_command(reset)
    cli_group.add_command(requeue)


def run(pipeline_func=None, confirm_func=None):

    _register_commands()

    cli_group(obj={
        "pipeline_func": pipeline_func,
        "confirm_func": confirm_func
    })
